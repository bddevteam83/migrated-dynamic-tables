# @bd-innovations/dynamic-tables
A bunch of dynamic tables based on [Angular Material Table](https://material.angular.io/components/table/)
for displaying different data with the same view pattern

## Table of contents
* [Installation](#installation)
* [Usage](#usage)
  * [Import the root module](#1-import-the-root-module)
  * [Configure columns for specific view](#2-configure-columns-for-specific-view)
  * [Call the table and in your component](#3-call-the-table-and-in-your-component)
* [Deprecated version](#deprecated-version)
* [WIKI](https://bitbucket.org/bddevteam83/dynamic-tables/wiki)

## Installation

First you need to install npm module:

```
npm i @bd-innovations/dynamic-tables --save
```

Also you have to manually install deep package dependencies:

```
npm i @angular/flex-layout --save
npm i @bd-innovations/component-collection --save
npm i @bd-innovations/directive-collection --save
npm i @bd-innovations/dynamic-form --save
npm i @bd-innovations/pipe-collection --save
npm i @ngx-translate/core lodash --save
```

Choose the version corresponding to your Angular version

| @angular packages | @bd-innovations/dynamic-tables | @bd-innovations/component-collection | @bd-innovations/dynamic-form |
| ----------------- | ------------------------------ | ------------------------------------ | ---------------------------- |
| 9                 | 0.4.x                          | 0.2.x                                | 0.1.x                        |
| 8                 | 0.3.x                          | 0.1.x                                | 0.0.x                        |
| 7                 | 0.3.x                          | 0.1.x                                | 0.0.x                        |

## Usage

#### 1. Import the root module:

   After success installation you need to import `TableModule.forRoot()` in the root NgModule of your application.
   
   This method allows you to configure icons and some other global things used by table (for more details see
   [ModuleConfig]()). Example below is designed for usage of
   [Material Design Icons](https://www.npmjs.com/package/@mdi/font)
   
```
import {NgModule} from '@angular/core';
...
import {TableModule} from '@bd-innovations/dynamic-tables';


@NgModule({
 imports: [
   TableModule.forRoot({
     globalIcons: {
       actions: 'mdi mdi-dots-vertical',
       bulkActions: 'mdi mdi-dots-horizontal',
       filter: 'mdi mdi-filter',
       expand: 'mdi mdi-chevron-down',
       displayedColumns: 'mdi mdi-view-parallel',
       displayedColumnsReorder: 'mdi mdi-drag-horizontal-variant',
       removeChip: 'mdi mdi-close-circle-outline',
     },
     actionIcons: {
       open: {iconClass: 'mdi mdi-dots-horizontal', label: 'Details'},
       edit: {iconClass: 'mdi mdi-pencil', label: 'Edit'},
       delete: {iconClass: 'mdi mdi-delete', label: 'Delete'},
       download: {iconClass: 'mdi mdi-download', label: 'Download'},
     },
     paginatorPageSizeOptions: [5, 10, 25, 50],
     globalTranslatePrefix: '',
     nullOrUndefinedPipePlaceholder: '-'
   }),
   ...
 ],
 ...
})
export class AppModule {
}
```

#### 2. Configure columns for specific view:

   In order to understand how you want to display data you need also configure and pass some metadata along with it to
   table.
   
   For example, if your data is an array of objects `{id: number, name: string}` your columns config may look something
   like this(for more details see [ColumnsConfig]()):
   
```
import {ColumnsV2Config} from '@bd-innovations/dynamic-tables';


const columns: ColumnsV2Config = {
id: {
  discriminator: 'TextColumn',
  paths: 'id'
},
name: {
  discriminator: 'TextColumn',
  paths: 'name'
}
}
```

#### 3. Call the table and in your component:

   At that's left is to call the table in your component and pass there appropriate data and columns config (Don't
   forget to import `TableModule` inside your component's NgModule if it's not the root NgModule, but now without
   `forRoot()`):
   
```
<bd-table [data]="data"
          [columns]="columns"></bd-table>
```

## Deprecated version

Fow now there is also deprecated version of table available with `MatDynamicTableModule.forRoot()` and
`<bd-mat-dynamic-table></bd-mat-dynamic-table>`. It is the reason that all configs of actual table version have V2 mark.
Please don't use this version since it will be deleted soon
