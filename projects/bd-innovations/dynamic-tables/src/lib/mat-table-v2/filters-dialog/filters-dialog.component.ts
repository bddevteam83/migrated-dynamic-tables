import {FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

export abstract class FiltersDialogComponent {

  form: FormGroup;

  protected constructor(
    protected dialogRef: MatDialogRef<FiltersDialogComponent>
  ) {}

  resetAndClose(): void {
    this.form.reset();
    this.dialogRef.close(this.form.value);
  }
}
