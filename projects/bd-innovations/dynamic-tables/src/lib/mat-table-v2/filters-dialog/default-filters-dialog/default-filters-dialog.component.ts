import {Component, Inject, OnInit, Optional} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {DynamicFormService} from '@bd-innovations/dynamic-form';
import {FiltersDialogDataModel} from '../filters-dialog-data.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FiltersDialogComponent} from '../filters-dialog.component';

@Component({
  selector: 'bd-default-filters-dialog',
  templateUrl: './default-filters-dialog.component.html',
  styleUrls: ['./default-filters-dialog.component.scss']
})
export class DefaultFiltersDialogComponent extends FiltersDialogComponent implements OnInit {

  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: FiltersDialogDataModel,
    protected dialogRef: MatDialogRef<DefaultFiltersDialogComponent>,
    @Optional() private dynamicFormService?: DynamicFormService
  ) {
    super(dialogRef);
  }

  ngOnInit(): void {
    this.form = this.dynamicFormService.generateForm(this.data.metadata, this.data.initValue);
  }

  resetAndClose(): void {
    this.form.reset();
    this.dialogRef.close(this.form.value);
  }
}
