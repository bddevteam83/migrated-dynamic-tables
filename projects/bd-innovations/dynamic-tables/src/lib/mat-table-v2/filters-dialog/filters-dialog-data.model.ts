import {GroupQuestion} from '@bd-innovations/dynamic-form';

export interface FiltersDialogDataModel {
  metadata: GroupQuestion;
  initValue?: any;
  additionalData?: any;
}
