import {EventEmitter, Injectable, Optional} from '@angular/core';

import {DomElementValidatorService} from '@bd-innovations/directive-collection';
import * as _ from 'lodash';

import {ActionConditionOperatorsEnum} from '../_shared/configs/action-condition.config';
import {ActionRequestV2Config} from '../_shared/configs/action-request-v2.config';
import {ActionsV2Config, ActionsV3Config} from '../_shared/configs/actions-v2.config';

@Injectable({providedIn: 'root'})
export class ActionsConfigService<T extends Record<string, any>> {
  actionRequest: EventEmitter<ActionRequestV2Config<T>> = new EventEmitter();
  set actions(actions: ActionsV2Config<T>) {
    this._actions = actions;
  }
  get actions(): ActionsV2Config<T> {
    return this._actions;
  }
  set bulkActions(bulkActions: string[]) {
    this._bulkActions = bulkActions;
  }
  get bulkActions(): string[] {
    return this._bulkActions;
  }
  hasBulkActions: boolean;
  hasHeaderButtons: boolean;
  hasSingleActions: boolean;
  set headerActions(headerActions: string[]) {
    this._headerActions = headerActions;
  }
  get headerActions(): string[] {
    return this._headerActions;
  }
  set singleActions(singleActions: string[]) {
    this._singleActions = singleActions;
  }
  get singleActions(): string[] {
    return this._singleActions;
  }
  private _actions: ActionsV2Config<T> = {};
  private _bulkActions: string[];
  private _headerActions: string[];
  private _singleActions: string[];

  constructor(
    @Optional() private readonly domElementValidatorService?: DomElementValidatorService,
  ) {}

  checkActions(actionsRequestObservsLength: boolean): void {
    let header = false;
    let bulk = false;
    let single = false;

    for (const action of Object.values(this.actions)) {
      header = header || action.type === 'header';
      bulk = bulk || action.type === 'bulk' || action.type === 'both';
      single = single || action.type === 'single' || action.type === 'both';
    }
    this.hasSingleActions = single && actionsRequestObservsLength;
    this.hasBulkActions = bulk && actionsRequestObservsLength;
    this.hasHeaderButtons = header && actionsRequestObservsLength;
  }

  filterBulkActions(_bulkActions: string[], actionConfig: ActionsV2Config<T>): any {
    return _bulkActions.filter(action => this.domElementValidatorService.validateRole(actionConfig[action].role));
  }

  filterSingleActions(actions: ActionsV2Config<T>, elem: T): string[] {
    return Object.keys(actions)
      .filter(key => this.checkActionType(actions[key]) &&
        this.checkActionRole(actions[key].role) &&
        this.checkActionCondition(elem, actions[key].condition));
  }

  initActions(actions: ActionsV2Config<T>): void {
    this.actions = actions;
    const bulk = [];
    const header = [];
    const single = [];

    Object.entries(actions)
      .forEach(([key, value]) => {
        switch (value.type) {
          case 'both':
          case 'bulk':
            bulk.push(key);
            break;
          case 'header':
            header.push(key);
            break;
          default:
            break;
        }
        if (value.type === 'single' || value.type === 'both'){
          single.push(key);
        }
      });
    this.singleActions = single;
    this.bulkActions = bulk;
    this.headerActions = header;
  }

  initFilterBulkActions(): void {
    this.bulkActions = this.filterBulkActions(this.bulkActions, this.actions);
    this.hasBulkActions = !!this.bulkActions.length;
  }
  // TODO delete ActionsV3Config when ActionsV2Config will be updated
  private checkActionCondition(elem: T, condition: (ActionsV2Config<T>|ActionsV3Config<T>)[keyof ActionsV2Config<T>]['condition']): boolean {
    if (!condition) {
      return true;
    }

    if (typeof condition === 'function') {
      return condition(elem);
    }

    const actualValue = _.at(elem as any, condition.key)[0];

    switch (condition.operator) {
      case ActionConditionOperatorsEnum.EQUALS:
        return actualValue === condition.value;
      case ActionConditionOperatorsEnum.TRUTHY:
        return !!actualValue;
      case ActionConditionOperatorsEnum.NOTEQUAL:
        return actualValue !== condition.value;
      case ActionConditionOperatorsEnum.IN:
        return condition.value.some(v => v === actualValue);
      default:
        break;
    }

    return false;
  }

  private checkActionRole(role: ActionsV2Config<T>[keyof ActionsV2Config<T>]['role']): boolean {
    if (!this.domElementValidatorService || !role || role instanceof Array && role.length === 0) {
      return true;
    }

    if (typeof role === 'string') {
      return this.domElementValidatorService.validateRole(role);
    } else if (role instanceof Array) {
      let checkRes = false;

      for (let index = 0; index < role.length; index++) {
        if (this.domElementValidatorService.validateRole(role[index])) {
          checkRes = true;
          break;
        }
      }

      return checkRes;
    }
  }

  private checkActionType(action: ActionsV2Config<T>[keyof ActionsV2Config<T>]): boolean {
    return action.type === 'single' || action.type === 'both';
  }
}
