import {TestBed} from '@angular/core/testing';

import {ActionsConfigService} from './actions-config.service';

describe('ActionsConfigService', () => {
  let service: ActionsConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ActionsConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
