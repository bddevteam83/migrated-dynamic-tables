// import {ComponentFixture, TestBed} from '@angular/core/testing';
// import {TableComponent} from './table.component';
// import {CommonModule} from '@angular/common';
// import {
//   MatCell,
//   MatCellDef,
//   MatColumnDef, MatFooterCell,
//   MatFooterCellDef, MatFooterRow, MatFooterRowDef, MatHeaderCell,
//   MatHeaderCellDef, MatHeaderRow,
//   MatHeaderRowDef, MatRow,
//   MatRowDef,
//   MatTable,
//   MatTableModule
// } from '@angular/material/table';
// import {MockComponent, MockDirective, MockPipe} from 'ng-mocks';
// import {
//   MatButton,
//   MatCheckbox,
//   MatIcon,
//   MatMenu, MatMenuTrigger,
//   MatPaginator,
//   MatProgressBar,
//   MatSort,
//   MatSortHeader,
//   MatTooltip
// } from '@angular/material';
// import {TranslatePipe} from '@ngx-translate/core';
// import {
//   BdDatePipe,
//   FirstLetterPipe,
//   NullOrUndefinedHandlerPipe,
//   OrderedKeyValuePipe,
//   RandomGradientPipe,
//   SliceWithDotsPipe,
//   TimeAgoPipe,
//   ToIconPipe
// } from '@bd-innovations/pipe-collection';
// import {RandomClassDirective, ShortPressDirective} from '@bd-innovations/directive-collection';
// import {FormsModule} from '@angular/forms';
// import {TableService} from '../table.service';
// import {TABLE_CONFIG} from '../_shared/utilities/table-tokens';
//
// describe('TableComponent', () => {
//   let component: TableComponent<any>;
//   let fixture: ComponentFixture<TableComponent<any>>;
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         CommonModule,
//         FormsModule,
//       ],
//       declarations: [
//         TableComponent,
//
//         MockComponent(MatTable),
//         MockComponent(MatPaginator),
//         MockComponent(MatButton),
//         MockComponent(MatMenu),
//         MockComponent(MatIcon),
//         MockComponent(MatCheckbox),
//         MockComponent(MatProgressBar),
//         MockComponent(MatSortHeader),
//
//         MockPipe(TranslatePipe),
//
//         MockPipe(TimeAgoPipe),
//         MockPipe(RandomGradientPipe),
//         MockPipe(NullOrUndefinedHandlerPipe),
//         MockPipe(SliceWithDotsPipe),
//         MockPipe(ToIconPipe),
//         MockPipe(BdDatePipe),
//         MockPipe(OrderedKeyValuePipe),
//         MockPipe(FirstLetterPipe),
//
//         MockDirective(MatMenuTrigger),
//         MockDirective(MatSort),
//         MockDirective(MatTooltip),
//         MockDirective(MatHeaderCellDef),
//         MockDirective(MatHeaderRowDef),
//         MockDirective(MatColumnDef),
//         MockDirective(MatCellDef),
//         MockDirective(MatRowDef),
//         MockDirective(MatFooterCellDef),
//         MockDirective(MatFooterRowDef),
//         MockDirective(MatHeaderCell),
//         MockDirective(MatCell),
//         MockDirective(MatFooterCell),
//         MockDirective(MatHeaderRow),
//         MockDirective(MatRow),
//         MockDirective(MatFooterRow),
//
//         MockDirective(ShortPressDirective),
//         MockDirective(RandomClassDirective),
//       ],
//       providers: [
//         {provide: TableService, useValue: {}},
//         {provide: TABLE_CONFIG, useValue: {}}
//       ]
//     }).compileComponents();
//
//     fixture = TestBed.createComponent(TableComponent);
//     component = fixture.componentInstance;
//   });
//
//   beforeEach(() => {
//   });
//
//   it('should be created', () => {
//     expect(component).toBeTruthy();
//   });
// });
