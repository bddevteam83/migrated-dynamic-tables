import {SelectionModel} from '@angular/cdk/collections';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  HostBinding,
  Inject,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  SimpleChanges,
  TemplateRef,
  Type,
  ViewChild,
} from '@angular/core';

import {FormControl} from '@angular/forms';
import {MatCheckbox} from '@angular/material/checkbox';
import {MatDialog} from '@angular/material/dialog';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ControlQuestion, DynamicFormService, GroupQuestion} from '@bd-innovations/dynamic-form';
import * as _ from 'lodash';
import {filter} from 'rxjs/operators';

import {TableToken} from '../_shared/abstract/table.token';
import {BaseColumnInterface} from '../_shared/components/data-column/base-column-cell/base-column.interface';
import {instanceOfProgressBarColumnInterface} from '../_shared/components/data-column/progress-bar-column-cell/progress-bar-column.interface';
import {instanceOfTextColumnInterface} from '../_shared/components/data-column/text-column-cell/text-column.interface';
import {ActionRequestV2Config} from '../_shared/configs/action-request-v2.config';
import {ActionsV2Config} from '../_shared/configs/actions-v2.config';
import {ColumnsV2Config} from '../_shared/configs/columns-v2.config';
import {ModuleV2Config} from '../_shared/configs/module-v2.config';
import {PaginationConfig} from '../_shared/configs/pagination.config';
import {ParsedObjectConfig} from '../_shared/configs/parsed-object.config';
import {TextSearchModeEnum} from '../_shared/enums/text-search-mode.enum';
import {ActiveFiltersPipe} from '../_shared/pipes/active-filters/active-filters.pipe';
import {EXPANDED_ROW_ANIMATION} from '../_shared/utilities/animations';
import {enumToArray} from '../_shared/utilities/enum-to-array.function';
import {mixinHasSubs} from '../_shared/utilities/has-subs.mixin';
import {ReservedNamesV2} from '../_shared/utilities/reserved-names-v2';
import {EXPANDED_ROW_DATA, TABLE, TABLE_CONFIG} from '../_shared/utilities/table-tokens';
import {DisplayedColumnsDialogDataModel} from '../displayed-columns-dialog/displayed-columns-dialog-data.model';
import {DisplayedColumnsDialogComponent} from '../displayed-columns-dialog/displayed-columns-dialog.component';
import {DefaultFiltersDialogComponent} from '../filters-dialog/default-filters-dialog/default-filters-dialog.component';
import {FiltersDialogDataModel} from '../filters-dialog/filters-dialog-data.model';
import {FiltersDialogComponent} from '../filters-dialog/filters-dialog.component';
import {TableHeaderDirective} from '../table-header/table-header.directive';
import {TableService} from '../table.service';
import {ActionsConfigService} from '../actions-config-service/actions-config.service';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'bd-table',
  templateUrl: './table.component.html',
  styleUrls: [
    '../_shared/styles/table.component.scss',
    '../../_shared/styles/gradients.scss',
    './table.component.scss',
  ],
  animations: [
    EXPANDED_ROW_ANIMATION,
  ],
  providers: [
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'legacy'}},
    {provide: TABLE, useExisting: TableComponent},
  ],
})
export class TableComponent<T extends Record<string, any>>
  extends mixinHasSubs(class {})
  implements
    OnChanges,
    OnInit,
    AfterViewInit,
    OnDestroy,
    TableToken<T> {

  @Input() set actions(actions: ActionsV2Config<T>) {
    this._actions = actions;
    const bulk = [];
    const header = [];

    Object.keys(actions).forEach(key => {

      switch (actions[key].type) {
        case 'both':
        case 'bulk':
          bulk.push(key);
          break;
        case 'header':
          header.push(key);
          break;
        default:
          break;
      }
    });
    this._bulkActions = bulk;
    this._headerActions = header;
  }
  get actions(): ActionsV2Config<T> {
    return this._actions;
  }
  @Input() additionalFiltersDialogData: any;
  @Input() set columns(columns: ColumnsV2Config) {
    this._columns = this.service.fillColumnsConfigDefaults(columns);

    this.displayedColumns = Object.keys(this._columns)
      .filter(key => !this._columns[key].hidden);

    this.textColumns = Object.keys(this._columns)
      .filter(key => instanceOfTextColumnInterface(this._columns[key]));

    this.progressBarColumns = Object.keys(this._columns)
      .filter(key => instanceOfProgressBarColumnInterface(this._columns[key]));

    this.textSearchableColumns = Object.entries(this._columns)
      .filter(([key, value]) => !!value.textSearch)
      .map(([key, value]) => key);

    this.textSearchEnabled = this.textSearchableColumns.length > 0;
  }
  @Input() customColumnTemplateRefs: {[key: string]: TemplateRef<any>;} = {};
  @Input() data: T[];
  @Input() expandedComponentRef: Type<any>;
  @Input() filtersDialogRef: Type<FiltersDialogComponent> = DefaultFiltersDialogComponent;
  @Input() globalSort = true;
  @Input() pagination: PaginationConfig;
  @Input() rowClickAction = 'open';
  @Input() textSearchPlaceholder = 'Search';
  @Input() title: string;
  @Input() translatePrefix = '';

  @Output() actionRequest: EventEmitter<ActionRequestV2Config<T>> = new EventEmitter();
  @Output() bulkActionRequest: EventEmitter<ActionRequestV2Config<T[]>> = new EventEmitter();
  @Output() displayedColumnsUpdate: EventEmitter<string[]> = new EventEmitter();

  @ViewChild('massBulkCheckbox') massBulkCheckbox: MatCheckbox;
  @ViewChild(MatPaginator) matPaginator: MatPaginator;
  @ViewChild(MatSort) matSort: MatSort;

  @ContentChild(TableHeaderDirective) tableHeader: TableHeaderDirective;

  _actions: ActionsV2Config<T> = {};
  _bulkActions: string[];
  _columns: ColumnsV2Config;
  _headerActions: string[];

  bulkSelection: SelectionModel<ParsedObjectConfig<T>> = new SelectionModel(true, []);

  @HostBinding('class.bd-table') componentClass = true;

  customHeader: boolean;
  dataSource: MatTableDataSource<ParsedObjectConfig<T>>;
  displayedColumns: string[];
  expandedComponentInjectors: Injector[];
  expandedElement: ParsedObjectConfig<T>;
  filtersMetadata: GroupQuestion;
  filtersValue: any;
  hasBulkActions: boolean;
  hasCursorPointer: boolean;
  hasDisplayedColumnsUpdate: boolean;
  hasExpandedRow: boolean;
  hasFilters: boolean;
  hasGlobalSort: boolean;
  hasHeader: boolean;
  hasHeaderButtons: boolean;
  hasPagination: boolean;
  hasSingleActions: boolean;
  progressBarColumns: string[] = [];
  reservedNames = ReservedNamesV2;
  textColumns: string[] = [];
  textSearchControl = new FormControl('');
  textSearchEnabled = false;

  private filterPredicate: (data: ParsedObjectConfig<T>, query: string) => boolean;
  private sortingDataAccessor: (data: ParsedObjectConfig<T>, property: string) => any;
  private textSearchableColumns: string[] = [];

  constructor(
    @Inject(TABLE_CONFIG) public config: ModuleV2Config,
    private readonly service: TableService<T>,
    private readonly actionsConfigService: ActionsConfigService<T>,
    private readonly injector: Injector,
    private readonly dialog: MatDialog,
    private readonly cd: ChangeDetectorRef,
    private readonly activeFiltersPipe: ActiveFiltersPipe,
    @Optional() private readonly dynamicFormService?: DynamicFormService,
  ) {
    super();
  }

  // LIFECYCLE HOOKS START

  ngOnChanges(changes: SimpleChanges): void {
    // need to exclude first change because setParams() need to be called first (in ngOnInit())
    if (changes.columns && !changes.columns.firstChange) {
      this.addSystemDisplayColumns();
    }

    if (changes.data && !changes.data.firstChange) {
      this.updateDataSource();
    }

    if (changes.actions && !changes.actions.firstChange) {
      this.filterBulkActions();
    }

    if (changes.pagination && !changes.pagination.firstChange) {
      this.initPagination();
      this.cd.detectChanges();
    }

    if (changes.data && this.hasExpandedRow) {
      this.initExpandedComponentInjectors();
    }
  }

  ngOnInit(): void {
    this.setFilterPredicate();
    this.setSortingDataAccessor();
    this.setParams();
    this.addSystemDisplayColumns();
    this.initDataSource();
    this.initTextSearch();

    if (this.hasBulkActions) {
      this.filterBulkActions();
    }

    if (this.hasFilters || this.textSearchEnabled) {
      this.dataSource.filterPredicate = this.filterPredicate;
      if (this.hasFilters) {
        this.initFilters();
      }
    }
  }

  ngAfterViewInit(): void {
    if (this.hasPagination) {
      this.initPagination();
    }

    if (this.hasGlobalSort) {
      this.initGlobalSort();
    }

    this.customHeader = !!this.tableHeader;

    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    this.clearSubscriptions();
  }

  // LIFECYCLE HOOKS END

  emitActionRequest(action: string, originalIndex: number): void {
    const req: ActionRequestV2Config<T> = {
      action,
      data: this.data[originalIndex],
    };

    this.actionRequest.emit(req);
  }

  emitBulkActionRequest(action: string): void {
    const req: ActionRequestV2Config<T[]> = {
      action,
      data: this.data
        .filter((elem, index) => this.bulkSelection.selected
          .map(parsedElem => parsedElem.originalIndex)
          .includes(index),
        ),
    };

    this.bulkActionRequest.emit(req);
  }

  emitHeaderActionRequest(action: string): void {
    this.actionRequest.emit({action, data: null});
  }

  isAllSelected(): boolean {
    const numSelected = this.bulkSelection.selected.length;
    const numRows = this.dataSource.data.length;

    return numSelected === numRows;
  }

  isCurrentPageSelected(): boolean {
    const pageIndex = this.dataSource.paginator.pageIndex;
    const pageSize = this.dataSource.paginator.pageSize;

    return this.dataSource.sortData(this.dataSource.filteredData, this.dataSource.sort)
      .slice(pageIndex * pageSize, pageIndex * pageSize + pageSize)
      .every(elem => this.bulkSelection.isSelected(elem));
  }

  onExpandArrowClicked(element: ParsedObjectConfig<T>): void {
    this.expandedElement = this.expandedElement === element ? null : element;
  }

  onRowClicked(element: ParsedObjectConfig<T>): void {
    if (element.singleActions.includes(this.rowClickAction)) {
      this.emitActionRequest(this.rowClickAction, element.originalIndex);
    }
  }

  openDisplayColumnsDialog(): void {
    const allColumns: string[] = Object.keys(this._columns);
    const currentDisplayColumns: string[] = this.displayedColumns.filter(elem => !enumToArray(ReservedNamesV2).includes(elem));

    const displayedColumnsDialogData: DisplayedColumnsDialogDataModel = {
      translatePrefix: this.translatePrefix,
      displayedColumns: currentDisplayColumns
        .map(elem => ({
          name: elem,
          isDisplayed: true,
        }))
        .concat(allColumns
          .filter(elem => !currentDisplayColumns.includes(elem))
          .map(elem => ({
            name: elem,
            isDisplayed: false,
          }))),
    };

    this.subs$.push(
      this.dialog.open<DisplayedColumnsDialogComponent, DisplayedColumnsDialogDataModel, string[]>(DisplayedColumnsDialogComponent, {
        data: displayedColumnsDialogData,
      }).afterClosed().pipe(
        filter(res => !!res),
      ).subscribe(res => {
        this.displayedColumnsUpdate.emit(res);
      }),
    );
  }

  openFiltersDialog(): void {
    this.subs$.push(
      this.dialog.open<FiltersDialogComponent, FiltersDialogDataModel, Record<string, any>>(this.filtersDialogRef, {
        data: {
          initValue: this.filtersValue,
          metadata: this.filtersMetadata,
          additionalData: this.additionalFiltersDialogData,
        },
      }).afterClosed().pipe(
        filter(res => !!res),
      ).subscribe(res => {
        this.updateFilterValue(res);
        this.dataSource.filter = JSON.stringify(this.filtersValue);
        this.dataSource.paginator.firstPage();
      }),
    );
  }

  removeFilter(control: string): void {
    this.filtersValue = Object.entries(this.filtersValue)
      .reduce((accumulator, [key, value]) => key === control ? accumulator : {...accumulator, [key]: value}, {});
    for (const name of this.textSearchableColumns) {
      if (!this.filtersValue[name]) {
        this.textSearchControl.setValue('');
      }
    }
    this.dataSource.filter = JSON.stringify(this.filtersValue);
    this.dataSource.paginator.firstPage();
  }

  toggleAll(): void {
    if (this.isAllSelected()) {
      this.bulkSelection.clear();
    } else {
      this.bulkSelection.select(...this.dataSource.data);
    }
  }

  toggleCurrentPage(): void {
    const pageIndex = this.dataSource.paginator.pageIndex;
    const pageSize = this.dataSource.paginator.pageSize;

    if (this.isCurrentPageSelected()) {
      this.dataSource.sortData(this.dataSource.filteredData, this.dataSource.sort)
        .slice(pageIndex * pageSize, pageIndex * pageSize + pageSize)
        .forEach(elem => {
          this.bulkSelection.deselect(elem);
        });
    } else {
      this.dataSource.sortData(this.dataSource.filteredData, this.dataSource.sort)
        .slice(pageIndex * pageSize, pageIndex * pageSize + pageSize)
        .forEach(elem => {
          this.bulkSelection.select(elem);
        });
    }
  }

  private addSystemDisplayColumns(): void {
    if (this.hasExpandedRow) {
      this.displayedColumns.unshift(ReservedNamesV2.expandArrowColumn);
    }
    if (this.hasBulkActions) {
      this.displayedColumns.unshift(ReservedNamesV2.bulkCheckboxColumn);
    }
    if (this.hasSingleActions) {
      this.displayedColumns.push(ReservedNamesV2.actionsColumn);
    }
  }

  private checkActions(): void {
    let header = false;
    let bulk = false;
    let single = false;

    for (const action of Object.values(this._actions)) {
      header = header || action.type === 'header';
      bulk = bulk || action.type === 'bulk' || action.type === 'both';
      single = single || action.type === 'single' || action.type === 'both';
    }
    this.hasSingleActions = single && !!this.actionRequest.observers.length;
    this.hasBulkActions = bulk && !!this.actionRequest.observers.length;
    this.hasHeaderButtons = header && !!this.actionRequest.observers.length;
  }

  private filterBulkActions(): void {
    this._bulkActions = this.actionsConfigService.filterBulkActions(this._bulkActions, this._actions);
  }

  private filterCellByTextSearch(column: BaseColumnInterface, columnContent: string, searchString: string): boolean | RegExpExecArray {
    if (!columnContent && searchString) {
      return false;
    }
    columnContent = String(columnContent);
    if (typeof column.textSearch === 'function') {
      return column.textSearch(columnContent);
    }
    switch (column.textSearch) {
      case TextSearchModeEnum.CONTAINS:
        return RegExp(searchString.toLowerCase()).exec(columnContent.toLowerCase());
      case TextSearchModeEnum.STARTS_WITH:
        return columnContent.toLowerCase().startsWith(searchString.toLowerCase());
      case TextSearchModeEnum.ENDS_WITH:
        return columnContent.toLowerCase().endsWith(searchString.toLowerCase());
      case TextSearchModeEnum.EQUAL_TO:
        return columnContent.toLowerCase() === searchString.toLowerCase();
      default: return false;
    }
  }

  private filterRowByTextSearch(data: ParsedObjectConfig<T>): boolean {
    if (!this.textSearchableColumns || !this.textSearchControl.value) {
      return true;
    }
    const searchString = this.textSearchControl.value;

    for (const name of this.textSearchableColumns) {
      const content = data[name].main;
      const column = this._columns[name];

      if (this.filterCellByTextSearch(column, content, searchString)) {
        return true;
      }
    }

    return false;
  }

  private initDataSource(): void {
    this.dataSource = new MatTableDataSource<ParsedObjectConfig<T>>(
      this.service.parseData(
        this.data,
        this._columns,
        this._actions,
      ),
    );

    if (this.hasExpandedRow) {
      this.initExpandedComponentInjectors();
    }
  }

  private initExpandedComponentInjectors(): void {
    this.expandedComponentInjectors = this.data.map(elem => Injector.create({
      providers: [
        {provide: EXPANDED_ROW_DATA, useValue: elem},
      ],
      parent: this.injector,
    }));
  }

  private initFilters(): void {
    this.filtersMetadata = {
      discriminator: 'group',
      direction: 'row',
      controls: _.mapValues(this._columns, column => ({
        discriminator: 'control',
        showTitle: true,
        control: column.control,
      } as ControlQuestion)),
    } as GroupQuestion;

    Object.keys(this.filtersMetadata.controls)
      .forEach(key => {
        if (!(this.filtersMetadata.controls[key] as ControlQuestion).control) {
          delete this.filtersMetadata.controls[key];
        }
      });
  }

  private initGlobalSort(): void {
    this.dataSource.sort = this.matSort;
    this.dataSource.sortingDataAccessor = this.sortingDataAccessor;
  }

  private initPagination(): void {
    this.matPaginator.pageIndex = this.pagination.pageIndex;
    this.matPaginator.pageSize = this.pagination.pageSize;
    this.matPaginator.pageSizeOptions = this.config.paginatorPageSizeOptions;

    if (this.hasPagination) {
      this.dataSource.paginator = this.matPaginator;
    }
  }

  private initTextSearch(): void {
    this.subs$.push(
      this.textSearchControl.valueChanges.subscribe(text => {
        if (!this.filtersValue) {
          this.filtersValue = {};
        }
        const searchFilter = {};

        for (const name of this.textSearchableColumns) {
          searchFilter[name] = text;
        }
        this.updateFilterValue(searchFilter);
        this.dataSource.filter = JSON.stringify(this.filtersValue);
      }),
    );
  }

  private setFilterPredicate(): void {
    this.filterPredicate = data => {
      if (!this.activeFiltersPipe.transform(this.filtersValue).length && !this.textSearchControl.value) {
        return true;
      }

      const res: boolean[] = [];

      this.activeFiltersPipe.transform(this.filtersValue).forEach(key => {
        let index;

        switch (true) {
          case this.textColumns.includes(key):
            index = data[key].main;
            break;
          case this.progressBarColumns.includes(key):
            index = data[key].actual;
            break;
          default:
            index = data[key];
            break;
        }

        switch (this._columns[key].control.discriminator) {
          case 'selection-list':
            res.push(this.filtersValue[key].includes(index));
            break;
          case 'textbox':
            res.push(index.includes(this.filtersValue[key]));
            break;
          case 'dropdown':
            res.push(index === this.filtersValue[key]);
            break;
          case 'chip-list-autocomplete':
            res.push(this.filtersValue[key].includes(index));
            break;
          default:
            break;
        }
      });
      res.push(!!this.filterRowByTextSearch(data));

      return res.every(elem => elem);
    };
  }

  private setParams(): void {
    this.checkActions();
    this.hasPagination = !!this.pagination;
    this.hasGlobalSort = !!this.globalSort;
    this.hasExpandedRow = !!this.expandedComponentRef;
    this.hasFilters = !!_.map(this._columns, 'control').filter(elem => elem).length ||
      !(this.filtersDialogRef.toString() === DefaultFiltersDialogComponent.toString());
    this.hasDisplayedColumnsUpdate = !!this.displayedColumnsUpdate.observers.length;
    this.hasCursorPointer = !!(this._actions[this.rowClickAction] && this.hasSingleActions);
    this.hasHeader = !!this.title || this.hasBulkActions || this.hasFilters ||
      this.hasDisplayedColumnsUpdate || this.hasHeaderButtons || this.textSearchEnabled;
  }

  private setSortingDataAccessor(): void {
    this.sortingDataAccessor = (data, property) => {
      switch (true) {
        case this.textColumns.includes(property):
          return data[property].main;
        case this.progressBarColumns.includes(property):
          return data[property].actual;
        default:
          return data[property];
      }
    };
  }

  private updateDataSource(): void {
    this.dataSource.data = this.service.parseData(
      this.data,
      this._columns,
      this._actions,
    );
  }

  private updateFilterValue(filters: any): void {
    this.filtersValue = {
      ...this.filtersValue,
      ...filters,
    };
  }
}
