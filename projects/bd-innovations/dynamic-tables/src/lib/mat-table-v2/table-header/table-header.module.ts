import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {TableInitStatePlaceholderDirective} from '../_shared/directives/table-init-state-placeholder.directive';
import {TableNoResultPlaceholderDirective} from '../_shared/directives/table-no-result-placeholder.directive';

import {TableHeaderDirective} from './table-header.directive';

@NgModule({
  declarations: [
    TableHeaderDirective,
    TableInitStatePlaceholderDirective,
    TableNoResultPlaceholderDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    TableHeaderDirective,
    TableInitStatePlaceholderDirective,
    TableNoResultPlaceholderDirective,
  ],
})
export class TableHeaderModule {}
