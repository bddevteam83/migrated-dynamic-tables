import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';

import {MatChipsModule} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {_MatMenuDirectivesModule, MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {DomElementValidatorDirectiveModule} from '@bd-innovations/directive-collection';
import {TranslateModule} from '@ngx-translate/core';

import {ActiveFiltersModule} from '../../_shared/pipes/active-filters/active-filters.module';
import {FilterActionsModule} from '../../_shared/pipes/filter-actions/filter-actions.module';

import {DefaultTableHeaderComponent} from './default-table-header.component';

@NgModule({
  declarations: [DefaultTableHeaderComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatTooltipModule,
    TranslateModule,
    MatButtonModule,
    FilterActionsModule,
    DomElementValidatorDirectiveModule,
    MatChipsModule,
    _MatMenuDirectivesModule,
    MatMenuModule,
    ActiveFiltersModule,
  ],
  exports: [DefaultTableHeaderComponent],
})
export class DefaultTableHeaderModule {}
