import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DefaultTableHeaderComponent } from './default-table-header.component';

describe('DefaultTableHeaderComponent', () => {
  let component: DefaultTableHeaderComponent;
  let fixture: ComponentFixture<DefaultTableHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultTableHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultTableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
