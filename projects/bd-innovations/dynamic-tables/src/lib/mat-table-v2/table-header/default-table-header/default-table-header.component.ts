import {Component, Inject, Input, OnInit} from '@angular/core';
import {TableToken} from '../../_shared/abstract/table.token';
import {TABLE, TABLE_CONFIG} from '../../_shared/utilities/table-tokens';
import {ModuleV2Config} from '../../_shared/configs/module-v2.config';

@Component({
  selector: 'bd-default-table-header',
  templateUrl: './default-table-header.component.html',
  styleUrls: ['./default-table-header.component.scss']
})
export class DefaultTableHeaderComponent<T extends object> implements OnInit {

  @Input() set bulkActionsShowLimit(limit) {
    this._bulkButtonsLimit = limit - 1;
  }

  get bulkButtonsLimit() {
    return this._bulkButtonsLimit
  }

  private _bulkButtonsLimit = 2;

  constructor(
    @Inject(TABLE) public readonly table: TableToken<T>,
    @Inject(TABLE_CONFIG) public readonly config: ModuleV2Config,
  ) {
  }

  ngOnInit(): void {
  }

}
