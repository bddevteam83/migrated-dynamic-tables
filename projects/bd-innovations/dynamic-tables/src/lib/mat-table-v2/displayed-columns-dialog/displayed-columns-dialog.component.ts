import {Component, Inject} from '@angular/core';
import {DisplayedColumnConfig, DisplayedColumnsDialogDataModel} from './displayed-columns-dialog-data.model';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {TABLE_CONFIG} from '../_shared/utilities/table-tokens';
import {ModuleV2Config} from '../_shared/configs/module-v2.config';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'bd-displayed-columns-dialog',
  templateUrl: './displayed-columns-dialog.component.html',
  styleUrls: ['./displayed-columns-dialog.component.scss']
})
export class DisplayedColumnsDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DisplayedColumnsDialogDataModel,
              @Inject(TABLE_CONFIG) public config: ModuleV2Config,
              private dialogRef: MatDialogRef<DisplayedColumnsDialogComponent>) {
  }

  reorderColumns({previousIndex, currentIndex}: CdkDragDrop<DisplayedColumnConfig>) {
    moveItemInArray(this.data.displayedColumns, previousIndex, currentIndex);
  }

  submit(data: DisplayedColumnConfig[]) {
    this.dialogRef.close(data
      .filter(elem => elem.isDisplayed)
      .map(elem => elem.name)
    );
  }
}
