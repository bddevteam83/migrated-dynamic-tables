export interface DisplayedColumnsDialogDataModel {
  displayedColumns: DisplayedColumnConfig[];
  translatePrefix: string
}

export interface DisplayedColumnConfig {
  name: string;
  isDisplayed: boolean;
}
