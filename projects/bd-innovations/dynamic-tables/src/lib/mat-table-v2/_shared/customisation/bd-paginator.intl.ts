import {Injectable} from '@angular/core';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {TranslateService} from '@ngx-translate/core';
import {take} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BdPaginatorIntl extends MatPaginatorIntl {

  constructor(private readonly translate: TranslateService) {
    super();

    this.getAndInitTranslations();
  }

  getAndInitTranslations() {
    this.translate.get(['rows-per-page', 'next-page', 'previous-page']).pipe(
      take(1)
    ).subscribe(res => {
      this.itemsPerPageLabel = res['rows-per-page'] + ':';
      this.nextPageLabel = res['next-page'];
      this.previousPageLabel = res['previous-page'];

      this.changes.next();
    });
  }
}
