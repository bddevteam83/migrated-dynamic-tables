import {TypedInterface} from '../../../utilities/typed-interface';
import {AnyTemplateQuestion} from '@bd-innovations/dynamic-form';
import {TextSearchModeEnum} from '../../../enums/text-search-mode.enum';

export interface BaseColumnInterface extends TypedInterface {
  paths: string | string[];
  control?: AnyTemplateQuestion<any>;
  hidden?: boolean;
  sortable?: boolean;
  dynamicFieldIndex?: number;
  textSearch?: null | TextSearchModeEnum | ((item: any) => boolean);
}
