import {Component, Inject, Input} from '@angular/core';
import {BaseColumnCellComponent} from '../base-column-cell/base-column-cell.component';
import {ProgressBarColumnInterface, ProgressBarColumnValue} from './progress-bar-column.interface';
import {TABLE_CONFIG} from '../../../utilities/table-tokens';
import {ModuleV2Config} from '../../../configs/module-v2.config';

@Component({
  selector: 'bd-progress-bar-column-cell',
  templateUrl: './progress-bar-column-cell.component.html',
  styleUrls: ['./progress-bar-column-cell.component.css']
})
export class ProgressBarColumnCellComponent implements BaseColumnCellComponent<ProgressBarColumnValue> {

  @Input() cellValue: ProgressBarColumnValue;
  @Input() columnConfig: ProgressBarColumnInterface;

  constructor(
    @Inject(TABLE_CONFIG) public config: ModuleV2Config,
  ) {
  }

}
