import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TextColumnCellComponent } from './text-column-cell.component';

describe('TextColumnComponent', () => {
  let component: TextColumnCellComponent;
  let fixture: ComponentFixture<TextColumnCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TextColumnCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextColumnCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
