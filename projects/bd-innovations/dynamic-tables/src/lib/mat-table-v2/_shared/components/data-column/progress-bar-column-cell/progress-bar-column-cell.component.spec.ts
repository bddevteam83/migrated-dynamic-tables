import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProgressBarColumnCellComponent } from './progress-bar-column-cell.component';

describe('ProgressBarColumnComponent', () => {
  let component: ProgressBarColumnCellComponent;
  let fixture: ComponentFixture<ProgressBarColumnCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressBarColumnCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarColumnCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
