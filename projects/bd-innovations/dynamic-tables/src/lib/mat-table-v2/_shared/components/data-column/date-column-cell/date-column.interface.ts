import {BaseColumnInterface} from '../base-column-cell/base-column.interface';

export interface DateColumnInterface extends BaseColumnInterface {
  discriminator: 'DateColumn';
  paths: string;
  format?: string;
  timeAgo?: boolean;
}

export type DateColumnValue = any;

export function instanceOfDateColumnInterface(object: any): object is DateColumnInterface {
  return object.discriminator === 'DateColumn';
}
