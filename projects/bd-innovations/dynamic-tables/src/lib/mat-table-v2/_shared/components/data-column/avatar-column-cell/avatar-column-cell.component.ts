import {Component, Inject, Input} from '@angular/core';
import {BaseColumnCellComponent} from '../base-column-cell/base-column-cell.component';
import {AvatarColumnInterface, AvatarColumnValue} from './avatar-column.interface';
import {TABLE_CONFIG} from '../../../utilities/table-tokens';
import {ModuleV2Config} from '../../../configs/module-v2.config';

@Component({
  selector: 'bd-avatar-column-cell',
  templateUrl: './avatar-column-cell.component.html',
  styleUrls: ['./avatar-column-cell.component.scss']
})
export class AvatarColumnCellComponent implements BaseColumnCellComponent<AvatarColumnValue> {

  @Input() cellValue: AvatarColumnValue;
  @Input() columnConfig: AvatarColumnInterface;

  constructor(
    @Inject(TABLE_CONFIG) public config: ModuleV2Config,
  ) {
  }

}
