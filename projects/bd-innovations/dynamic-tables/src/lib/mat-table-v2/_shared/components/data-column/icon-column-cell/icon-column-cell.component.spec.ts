import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IconColumnCellComponent } from './icon-column-cell.component';

describe('IconColumnComponent', () => {
  let component: IconColumnCellComponent;
  let fixture: ComponentFixture<IconColumnCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IconColumnCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconColumnCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
