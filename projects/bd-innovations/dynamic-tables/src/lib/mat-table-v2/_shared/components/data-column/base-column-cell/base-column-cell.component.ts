import {BaseColumnInterface} from './base-column.interface';

export interface BaseColumnCellComponent<T> {
  cellValue: T;
  columnConfig: BaseColumnInterface;
}
