import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DateColumnCellComponent } from './date-column-cell.component';

describe('DateColumnComponent', () => {
  let component: DateColumnCellComponent;
  let fixture: ComponentFixture<DateColumnCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DateColumnCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateColumnCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
