import {Component, Inject, Input} from '@angular/core';
import {TABLE_CONFIG} from '../../../utilities/table-tokens';
import {ModuleV2Config} from '../../../configs/module-v2.config';
import {TextColumnInterface, TextColumnValue} from './text-column.interface';
import {BaseColumnCellComponent} from '../base-column-cell/base-column-cell.component';

@Component({
  selector: 'bd-text-column-cell',
  templateUrl: './text-column-cell.component.html',
  styleUrls: ['./text-column-cell.component.scss']
})
export class TextColumnCellComponent implements BaseColumnCellComponent<TextColumnValue> {

  @Input() cellValue: TextColumnValue;
  @Input() columnConfig: TextColumnInterface;

  constructor(
    @Inject(TABLE_CONFIG) public config: ModuleV2Config,
  ) {
  }

}
