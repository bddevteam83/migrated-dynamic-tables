import {BaseColumnInterface} from '../base-column-cell/base-column.interface';

export interface TextColumnInterface extends BaseColumnInterface {
  discriminator: 'TextColumn';
  subPath?: string;
  limit?: number;
  translatePrefix?: string;
  subHeaderKey?: string;
  separator?: string | string[];
  mask?: (elem: string) => string;
}

export interface TextColumnValue {
  main: string;
  sub?: string;
}

export function instanceOfTextColumnInterface(object: any): object is TextColumnInterface {
  return object.discriminator === 'TextColumn';
}
