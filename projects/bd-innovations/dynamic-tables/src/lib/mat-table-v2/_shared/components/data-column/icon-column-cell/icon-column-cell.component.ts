import {Component, EventEmitter, Inject, Input, Output} from '@angular/core';
import {BaseColumnCellComponent} from '../base-column-cell/base-column-cell.component';
import {IconColumnInterface, IconColumnValue} from './icon-column.interface';
import {TABLE_CONFIG} from '../../../utilities/table-tokens';
import {ModuleV2Config} from '../../../configs/module-v2.config';

@Component({
  selector: 'bd-icon-column-cell',
  templateUrl: './icon-column-cell.component.html',
  styleUrls: ['./icon-column-cell.component.scss']
})
export class IconColumnCellComponent implements BaseColumnCellComponent<IconColumnValue> {

  @Input() cellValue: IconColumnValue;
  @Input() columnConfig: IconColumnInterface;

  @Output() actionRequest: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    @Inject(TABLE_CONFIG) public config: ModuleV2Config,
  ) {
  }

}
