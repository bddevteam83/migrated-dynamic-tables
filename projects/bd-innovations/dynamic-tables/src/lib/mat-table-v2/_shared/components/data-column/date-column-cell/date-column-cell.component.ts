import {Component, Inject, Input} from '@angular/core';
import {BaseColumnCellComponent} from '../base-column-cell/base-column-cell.component';
import {DateColumnInterface, DateColumnValue} from './date-column.interface';
import {TABLE_CONFIG} from '../../../utilities/table-tokens';
import {ModuleV2Config} from '../../../configs/module-v2.config';

@Component({
  selector: 'bd-date-column-cell',
  templateUrl: './date-column-cell.component.html',
  styleUrls: ['./date-column-cell.component.scss']
})
export class DateColumnCellComponent implements BaseColumnCellComponent<DateColumnValue> {

  @Input() columnConfig: DateColumnInterface;
  @Input() cellValue: DateColumnValue;

  constructor(
    @Inject(TABLE_CONFIG) public config: ModuleV2Config,
  ) {
  }


}
