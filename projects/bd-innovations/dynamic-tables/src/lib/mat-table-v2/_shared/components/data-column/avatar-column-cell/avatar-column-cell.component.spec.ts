import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AvatarColumnCellComponent } from './avatar-column-cell.component';

describe('AvatarColumnComponent', () => {
  let component: AvatarColumnCellComponent;
  let fixture: ComponentFixture<AvatarColumnCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AvatarColumnCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarColumnCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
