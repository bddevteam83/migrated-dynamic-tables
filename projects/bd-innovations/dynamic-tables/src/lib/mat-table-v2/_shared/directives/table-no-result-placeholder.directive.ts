import { Directive } from '@angular/core';

@Directive({
  selector: '[bdTableNoResultPlaceholder]'
})
export class TableNoResultPlaceholderDirective {

  constructor() { }

}
