import {SelectionModel} from '@angular/cdk/collections';
import {EventEmitter} from '@angular/core';

import {FormControl} from '@angular/forms';

import {ActionRequestV2Config} from '../configs/action-request-v2.config';
import {ActionsV2Config} from '../configs/actions-v2.config';
import {ColumnsV2Config} from '../configs/columns-v2.config';

import {ParsedObjectConfig} from '../configs/parsed-object.config';

// eslint-disable-next-line @typescript-eslint/ban-types
export interface TableToken<T extends object> {
  columns: ColumnsV2Config;

  actions: ActionsV2Config<T>;
  actionRequest: EventEmitter<ActionRequestV2Config<T>>;
  bulkSelection: SelectionModel<ParsedObjectConfig<T>>;
  bulkActionRequest: EventEmitter<ActionRequestV2Config<T[]>>;

  textSearchEnabled: boolean;
  textSearchPlaceholder: string;
  textSearchControl: FormControl;

  hasDisplayedColumnsUpdate: boolean;

  hasFilters: boolean;
  filtersValue: any;
  hasBulkActions: boolean;

  translatePrefix: string;

  openFiltersDialog: () => void;
  openDisplayColumnsDialog: () => void;
  emitHeaderActionRequest: (action: string) => void;
  removeFilter: (control: string) => void;
  emitBulkActionRequest: (action: string) => void;
}
