import {Subscription} from 'rxjs';
import {Constructor} from '@angular/material/core/common-behaviors/constructor';

export interface HasSubs {
  subs$: Subscription[];

  clearSubscriptions(): void;
}

export type HasSubsCtor = Constructor<HasSubs>;

export function mixinHasSubs<B extends Constructor<{}>>(base: B): HasSubsCtor & B {
  return class extends base {

    subs$: Subscription[] = [];

    constructor(...args: any[]) {
      super(args);
    }

    clearSubscriptions(): void {
      this.subs$.forEach(sub => sub.unsubscribe());
    }

  };
}
