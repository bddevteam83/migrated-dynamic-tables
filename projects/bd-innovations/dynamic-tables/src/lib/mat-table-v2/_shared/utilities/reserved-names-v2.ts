export enum ReservedNamesV2 {
  expandArrowColumn = '__expandArrowColumn__',
  expandedColumn = '__expandedColumnColumn__',
  actionsColumn = '__actionsColumnColumn__',
  bulkCheckboxColumn = '__bulkCheckboxColumn__',
  bulkMassCheckboxColumn = '__bulkMassCheckboxColumn__',
  infoColumn = '__infoColumn__',
}
