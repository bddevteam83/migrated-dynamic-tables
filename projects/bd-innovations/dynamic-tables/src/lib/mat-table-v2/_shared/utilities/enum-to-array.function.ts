export function enumToArray<T>(enumRef: any): T[] {
  const res: T[] = [];

  for (const key in enumRef) {
    res.push(enumRef[key] as T);
  }

  return res;
}
