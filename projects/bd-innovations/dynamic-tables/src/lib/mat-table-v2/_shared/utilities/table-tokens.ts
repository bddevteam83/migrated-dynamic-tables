import {InjectionToken} from '@angular/core';
import {TableToken} from '../abstract/table.token';
import {ModuleV2Config} from '../configs/module-v2.config';

export const EXPANDED_ROW_DATA: InjectionToken<any> = new InjectionToken<any>('EXPANDED_ROW_DATA');

export const TABLE_CONFIG: InjectionToken<ModuleV2Config> = new InjectionToken<ModuleV2Config>('TABLE_CONFIG');

export const TABLE: InjectionToken<object> = new InjectionToken<TableToken<object>>('TABLE');
