export enum TextSearchModeEnum {
  EQUAL_TO = 'EQUAL_TO',
  CONTAINS = 'CONTAINS',
  STARTS_WITH = 'STARTS_WITH',
  ENDS_WITH = 'ENDS_WITH'
}
