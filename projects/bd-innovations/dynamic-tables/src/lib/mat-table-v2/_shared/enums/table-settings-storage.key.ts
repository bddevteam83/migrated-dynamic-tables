export enum TableSettingsStorageKey {
  FILTERS = 'filters',
  PAGINATION = 'pagination',
  SORT = 'sort',
  COLUMNS = 'columns',
}
