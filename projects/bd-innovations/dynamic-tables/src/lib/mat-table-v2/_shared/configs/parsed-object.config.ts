export interface ParsedObjectConfig<T> {
  originalIndex: number;
  singleActions: string[];

  [key: string]: any;
}
