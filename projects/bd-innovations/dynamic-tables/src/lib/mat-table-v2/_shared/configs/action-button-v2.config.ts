export interface ActionButtonV2Config {
  iconClass: string;
  label: string;
}
