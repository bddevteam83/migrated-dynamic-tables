import {TableSettingsStorageKey} from '../enums/table-settings-storage.key';

export type StoredSettingsConfig  = Record<TableSettingsStorageKey, Storage>;
