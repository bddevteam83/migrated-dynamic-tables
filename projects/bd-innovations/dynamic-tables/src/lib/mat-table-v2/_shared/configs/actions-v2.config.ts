import {ActionButtonV2Config} from './action-button-v2.config';
import {ActionConditionConfig} from './action-condition.config';

export interface ActionsV2Config<T> { // NOTE <T> - is not used
  [key: string]: {
    type: 'single' | 'bulk' | 'both' | 'header';
    condition?: ActionConditionConfig;
    role?: string | string[];
    custom?: ActionButtonV2Config
  };
}

// TODO delete ActionsV3Config when ActionsV2Config will be updated
export interface ActionsV3Config<T> {
  [key: string]: {
    condition?: (elem: T) => boolean;
    custom?: ActionButtonV2Config;
    role?: string | string[];
    type: 'single' | 'bulk' | 'both' | 'header';
  };
}
