export interface ActionConditionConfig {
  key: string;
  operator: string;
  value?: any;
}

export enum ActionConditionOperatorsEnum {
  EQUALS = 'EQUALS',
  NOTEQUAL = 'NOTEQUAL',
  TRUTHY = 'TRUTHY',
  IN = 'IN'
}
