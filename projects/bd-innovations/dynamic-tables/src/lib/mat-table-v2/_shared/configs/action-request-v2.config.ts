export interface ActionRequestV2Config<T> {
  action: string;
  data: T;
}
