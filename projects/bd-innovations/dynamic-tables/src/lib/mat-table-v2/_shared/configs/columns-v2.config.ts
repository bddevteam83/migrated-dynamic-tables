import {AvatarColumnInterface} from '../components/data-column/avatar-column-cell/avatar-column.interface';
import {DateColumnInterface} from '../components/data-column/date-column-cell/date-column.interface';
import {IconColumnInterface} from '../components/data-column/icon-column-cell/icon-column.interface';
import {TextColumnInterface} from '../components/data-column/text-column-cell/text-column.interface';
import {ProgressBarColumnInterface} from '../components/data-column/progress-bar-column-cell/progress-bar-column.interface';


export interface ColumnsV2Config {
  [key: string]: AvatarColumnInterface
    | DateColumnInterface
    | IconColumnInterface
    | ProgressBarColumnInterface
    | TextColumnInterface;
}
