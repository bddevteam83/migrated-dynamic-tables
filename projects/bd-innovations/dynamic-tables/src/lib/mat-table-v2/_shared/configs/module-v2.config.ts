import {ActionButtonV2Config} from './action-button-v2.config';

export interface ModuleV2Config {
  globalIcons: {
    actions: string;
    bulkActions: string;
    filter: string;
    expand: string;
    displayedColumns: string;
    displayedColumnsReorder: string;
    removeChip: string;
    // activeChip: string;
  };
  actionIcons: { [key: string]: ActionButtonV2Config };
  paginatorPageSizeOptions: number[];
  globalTranslatePrefix: string;
  nullOrUndefinedPipePlaceholder: string;
}
