import {Pipe, PipeTransform} from '@angular/core';
import {Icon} from '../../components/data-column/icon-column-cell/icon-column.interface';
import * as _ from 'lodash';

@Pipe({
  name: 'toLabel'
})
export class ToLabelPipe implements PipeTransform {

  transform(value: any, icons: Icon[]): string {
    if (value === null || undefined) {
      return null;
    }

    const res = icons.find(icon => _.isEqual(icon.case, value));

    return res ? res.label : null;
  }

}
