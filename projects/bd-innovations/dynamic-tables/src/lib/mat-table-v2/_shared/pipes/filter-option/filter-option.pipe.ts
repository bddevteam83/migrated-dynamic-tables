import {Pipe, PipeTransform} from '@angular/core';
import * as _ from 'lodash';
import { List, ListIterateeCustom, StringIterator, ListIteratorTypeGuard, ObjectIteratorTypeGuard, ObjectIterateeCustom } from 'lodash';

@Pipe({
  name: 'filterOption',
  pure: false
})
export class FilterOptionPipe implements PipeTransform {

  transform(collection: string | null | undefined, predicate?: StringIterator<boolean>): string[];
  transform<T, S extends T>(collection: List<T> | null | undefined, predicate: ListIteratorTypeGuard<T, S>): S[];
  transform<T>(collection: List<T> | null | undefined, predicate?: ListIterateeCustom<T, boolean>): T[];
  transform<T extends object, S extends T[keyof T]>(collection: T | null | undefined, predicate: ObjectIteratorTypeGuard<T, S>): S[];
  transform<T extends object>(collection: T | null | undefined, predicate?: ObjectIterateeCustom<T, boolean>): Array<T[keyof T]>;
  transform(collection, predicate?) {
    return _.filter(collection, predicate);
  }

}
