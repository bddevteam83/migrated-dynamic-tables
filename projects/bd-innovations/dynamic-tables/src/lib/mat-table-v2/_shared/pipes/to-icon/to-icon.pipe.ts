import {Pipe, PipeTransform} from '@angular/core';
import * as _ from 'lodash';
import {Icon} from '../../components/data-column/icon-column-cell/icon-column.interface';

// TODO cover with tests
@Pipe({
  name: 'toIcon'
})
export class ToIconPipe implements PipeTransform {

  transform(value: any, icons: Icon[]): string {
    if (value === null || undefined) {
      return null;
    }

    const res = icons.find(icon => _.isEqual(icon.case, value)) || icons.find(icon => icon.case === '__default__');

    return res ? res.icon : null;
  }

}
