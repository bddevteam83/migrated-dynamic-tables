import {NgModule} from '@angular/core';
import {ToIconPipe} from './to-icon.pipe';

@NgModule({
  declarations: [ToIconPipe],
  exports: [ToIconPipe]
})
export class ToIconModule {
}
