import {Pipe, PipeTransform} from '@angular/core';
import {ActionsV2Config} from '../../configs/actions-v2.config';

@Pipe({
  name: 'filterActions'
})
export class FilterActionsPipe<T> implements PipeTransform {

  transform(
    actions: ActionsV2Config<T>,
    type: 'single' | 'bulk' | 'header'
  ): string[] {
    if (!actions) {
      return [];
    }

    return Object.entries(actions)
      .filter(([key, value]) => {
        if (type === 'header') {
          return value.type === type;
        } else {
          return value.type === type || value.type === 'both';
        }
      })
      .map(([key, value]) => key);
  }

}
