import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'activeFilters'
})
export class ActiveFiltersPipe implements PipeTransform {

  transform(value: object): string[] {
    if (!value) {
      return [];
    }
    return this.getActiveFilters(value);
  }

  private getActiveFilters(filtersValue: object): string[] {
    return Object.keys(filtersValue).filter(key => this.hasActiveFilter(filtersValue[key]));
  }

  private hasActiveFilter(filterValue: object): boolean {
    if (!filterValue) {
      return false;
    }
    return Object.values(filterValue).some(value => {
      if (Array.isArray(value)) {
        return value.length > 0;
      }
      if (typeof value === 'object' && value !== null) {
        return this.hasActiveFilter(value);
      }
      return !!value;
    });
  }
}
