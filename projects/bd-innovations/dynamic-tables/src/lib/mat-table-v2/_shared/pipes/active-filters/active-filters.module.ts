import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActiveFiltersPipe} from './active-filters.pipe';


@NgModule({
  declarations: [ActiveFiltersPipe],
  imports: [
    CommonModule
  ],
  exports: [ActiveFiltersPipe]
})
export class ActiveFiltersModule {
}
