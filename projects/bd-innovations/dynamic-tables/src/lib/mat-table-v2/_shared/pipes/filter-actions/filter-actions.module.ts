import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterActionsPipe } from './filter-actions.pipe';



@NgModule({
  declarations: [FilterActionsPipe],
  imports: [
    CommonModule
  ],
  exports: [FilterActionsPipe]
})
export class FilterActionsModule { }
