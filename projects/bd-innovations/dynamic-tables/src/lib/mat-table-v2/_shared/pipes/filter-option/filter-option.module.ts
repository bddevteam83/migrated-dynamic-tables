import {NgModule} from '@angular/core';
import {FilterOptionPipe} from './filter-option.pipe';

@NgModule({
  declarations: [FilterOptionPipe],
  exports: [FilterOptionPipe]
})
export class FilterOptionModule {
}
