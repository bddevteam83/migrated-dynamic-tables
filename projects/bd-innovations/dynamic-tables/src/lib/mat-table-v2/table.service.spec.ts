// import {TestBed} from '@angular/core/testing';
// import {TableService} from './table.service';
// import * as faker from 'faker';
// import {ColumnsV2Config} from './_shared/configs/columns-v2.config';
// import {ConditionalActionModel} from '../mat-dynamic-table/configs/actions.config';
//
// describe('TableService', () => {
//   let service: TableService<any>;
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       providers: [TableService]
//     });
//     service = TestBed.inject(TableService);
//   });
//
//   it('should be created', () => {
//     expect(service).toBeTruthy();
//   });
//
//   describe('#parseData(data: T[], columns: ColumnsV2Config, actions: string[], conditionalActions: ConditionalActionModel[], roles: RolesConfig)', () => {
//     it('should cycle through data argument and map it with #defineParsedElemSystemProperties() and #defineParsedElemDataProperties() results', () => {
//       const dataMock: any[] = Array.apply(null, {length: 10})
//         .map((elem, index) => index);
//       const actionsMock: any = 'actions mock';
//       const conditionalActionsMock: any = [{type: 'conditional action mock'}];
//       const rolesMock: any = 'roles mock';
//       const columnsMock: any = {column: 'columns mock'};
//
//       const defineParsedElemSystemPropertiesSpy = spyOn((service as any), 'defineParsedElemSystemProperties')
//         .and.callFake((elem, originalIndex, actions, conditionalActions, roles) => {
//           expect(dataMock[originalIndex]).toEqual(elem);
//           expect(actions).toEqual(actionsMock);
//           expect(conditionalActions.map(conAct => conAct.type)).toEqual(conditionalActionsMock.map(conAct => conAct.type));
//           expect(roles).toEqual(rolesMock);
//           return {
//             systemProp: true
//           };
//         });
//       const defineParsedElemDataPropertiesSpy = spyOn((service as any), 'defineParsedElemDataProperties')
//         .and.callFake((elem, columns, keys) => {
//           expect(dataMock.includes(elem)).toBeTruthy();
//           expect(columns).toEqual(columnsMock);
//           expect(keys).toEqual(Object.keys(columnsMock));
//           return {
//             dataProp: true
//           };
//         });
//
//       const res = service.parseData(dataMock, columnsMock, actionsMock, conditionalActionsMock, rolesMock,);
//
//       expect(res).toEqual(dataMock.map(() => ({
//         systemProp: true,
//         dataProp: true
//       })) as any);
//       expect(defineParsedElemSystemPropertiesSpy).toHaveBeenCalledTimes(dataMock.length);
//       expect(defineParsedElemDataPropertiesSpy).toHaveBeenCalledTimes(dataMock.length);
//     });
//   });
//
//   describe('(tests with element)', () => {
//     let elementMock: { [key: string]: any };
//
//     beforeEach(() => {
//       elementMock = {
//         firstProp: faker.random.word(),
//         secondProp: faker.random.word(),
//         nestedObject: {
//           nestedFirst: faker.random.word(),
//           nestedSecond: faker.random.word()
//         },
//         nestedArray: [
//           faker.random.number(100),
//           faker.date.past()
//         ]
//       };
//     });
//
//     describe('#defineParsedElemSystemProperties(elem: T, originalIndex: number, actions: string[], conditionalActions: ConditionalActionModel[], roles: RolesConfig)', () => {
//       it('should set system properties and return them as object', () => {
//         const actionsMock: string[] = [
//           'open',
//           'edit',
//         ];
//         const conditionalActionsMock: ConditionalActionModel<any>[] = [
//           {
//             type: 'delete',
//             condition: (elem) => elem.firstProp
//           },
//           {
//             type: 'commit',
//             condition: (elem) => elem.nonExistedProp
//           }
//         ];
//
//         const res = (service as any).defineParsedElemSystemProperties(elementMock, 2, actionsMock, conditionalActionsMock);
//
//         expect(res).toEqual({
//           originalIndex: 2,
//           selected: false,
//           singleActions: [
//             'open',
//             'edit',
//             'delete'
//           ],
//         });
//       });
//     });
//
//     describe('#defineParsedElemDataProperties(elem: T, columns: ColumnsV2Config, keys: string[])', () => {
//       it('should call proper parseFunctions and return object with props of their results', () => {
//         const columnsMock: ColumnsV2Config = {
//           nestedObject: {
//             discriminator: 'TextColumn',
//             paths: ['nestedObject.nestedFirst', 'nestedObject.nestedSecond']
//           },
//           nestedArray: {
//             discriminator: 'DateColumn',
//             paths: 'nestedArray.1'
//           },
//           firstProp: {
//             discriminator: 'AvatarColumn',
//             paths: ['firstProp', 'secondProp']
//           },
//           secondProp: {
//             discriminator: 'IconColumn',
//             paths: 'secondProp',
//             icons: []
//           },
//           otherName: {
//             discriminator: 'ProgressBarColumn',
//             paths: 'nestedArray.0',
//             maxValue: 100
//           }
//         };
//
//         const parseTextColumnSpy = spyOn((service as any), 'parseTextColumn')
//           .and.callFake((elem, column) => {
//             expect(elem).toEqual(elementMock);
//             expect(column).toEqual(columnsMock['nestedObject']);
//             return 'text parse result';
//           });
//         const parseDateColumnSpy = spyOn((service as any), 'parseDateColumn')
//           .and.callFake((elem, column) => {
//             expect(elem).toEqual(elementMock);
//             expect(column).toEqual(columnsMock['nestedArray']);
//             return 'date parse result';
//           });
//         const parseAvatarColumnSpy = spyOn((service as any), 'parseAvatarColumn')
//           .and.callFake((elem, column) => {
//             expect(elem).toEqual(elementMock);
//             expect(column).toEqual(columnsMock['firstProp']);
//             return 'avatar parse result';
//           });
//         const parseIconColumnSpy = spyOn((service as any), 'parseIconColumn')
//           .and.callFake((elem, column) => {
//             expect(elem).toEqual(elementMock);
//             expect(column).toEqual(columnsMock['secondProp']);
//             return 'icon parse result';
//           });
//         const parseProgressBarColumn = spyOn((service as any), 'parseProgressBarColumn')
//           .and.callFake((elem, column) => {
//             expect(elem).toEqual(elementMock);
//             expect(column).toEqual(columnsMock['otherName']);
//             return 'progress bar parse result';
//           });
//
//         const res = (service as any).defineParsedElemDataProperties(elementMock, columnsMock, Object.keys(columnsMock));
//
//         expect(parseTextColumnSpy).toHaveBeenCalledTimes(1);
//         expect(parseDateColumnSpy).toHaveBeenCalledTimes(1);
//         expect(parseAvatarColumnSpy).toHaveBeenCalledTimes(1);
//         expect(parseIconColumnSpy).toHaveBeenCalledTimes(1);
//         expect(parseProgressBarColumn).toHaveBeenCalledTimes(1);
//
//         expect(res).toEqual({
//           nestedObject: 'text parse result',
//           nestedArray: 'date parse result',
//           firstProp: 'avatar parse result',
//           secondProp: 'icon parse result',
//           otherName: 'progress bar parse result'
//         });
//       });
//
//       it('should throw en error when getting unknown column', () => {
//         // TODO implement this test
//       });
//     });
//
//     describe('#parseAvatarColumn(data: T, column: AvatarColumnInterface)', () => {
//       it('should return field value based on column', function () {
//         expect((service as any).parseAvatarColumn(elementMock, {
//           discriminator: 'AvatarColumn',
//           paths: 'firstProp'
//         })).toEqual(elementMock.firstProp[0]);
//       });
//     });
//
//     describe('#parseDateColumn(data: T, column: DateColumnInterface)', () => {
//       it('should return field value based on column', function () {
//         expect((service as any).parseDateColumn(elementMock, {
//           discriminator: 'DateColumn',
//           paths: 'nestedArray.1'
//         })).toEqual(elementMock.nestedArray[1]);
//       });
//     });
//
//     describe('#parseIconColumn(data: T, column: IconColumnInterface)', () => {
//       it('should return field value based on column', function () {
//         expect((service as any).parseIconColumn(elementMock, {
//           discriminator: 'IconColumn',
//           paths: 'secondProp',
//           icons: []
//         })).toEqual(elementMock.secondProp);
//       });
//     });
//
//     describe('#parseProgressBarColumn(data: T, column: ProgressBarColumnInterface)', () => {
//       it('should return field value based on column', function () {
//         expect((service as any).parseProgressBarColumn(elementMock, {
//           discriminator: 'ProgressBarColumn',
//           paths: 'nestedArray.0',
//           maxValue: 100
//         })).toEqual(elementMock.nestedArray[0]);
//       });
//     });
//
//     describe('#parseTextColumn(data: T, column: TextColumnInterface)', () => {
//       it('should return array of field values based on column', () => {
//         expect((service as any).parseTextColumn(elementMock, {
//           discriminator: 'TextColumn',
//           paths: 'firstProp'
//         })).toEqual({main: elementMock.firstProp, sub: undefined});
//
//         expect((service as any).parseTextColumn(elementMock, {
//           discriminator: 'TextColumn',
//           paths: 'fieldThree',
//         })).toEqual({main: null, sub: undefined});
//
//         expect((service as any).parseTextColumn(elementMock, {
//           discriminator: 'TextColumn',
//           paths: 'firstProp',
//           mask: value => value[2]
//         })).toEqual({main: elementMock.firstProp[2], sub: undefined});
//
//         expect((service as any).parseTextColumn(elementMock, {
//           discriminator: 'TextColumn',
//           paths: ['firstProp', 'nestedObject.nestedFirst', 'nestedObject.nestedSecond'],
//           separator: ', '
//         })).toEqual({
//           main: `${elementMock.firstProp}, ${elementMock.nestedObject.nestedFirst}, ${elementMock.nestedObject.nestedSecond}`,
//           sub: undefined
//         });
//
//         expect((service as any).parseTextColumn(elementMock, {
//           discriminator: 'TextColumn',
//           paths: ['firstProp', 'nestedObject.nestedFirst', 'nestedObject.nestedSecond'],
//           separator: [' - ', ', ']
//         })).toEqual({
//           main: `${elementMock.firstProp} - ${elementMock.nestedObject.nestedFirst}, ${elementMock.nestedObject.nestedSecond}`,
//           sub: undefined
//         });
//
//         expect((service as any).parseTextColumn(elementMock, {
//           discriminator: 'TextColumn',
//           paths: ['firstProp', 'nestedObject.nestedFirst', 'nestedObject.nestedSecond'],
//           separator: [' - ', ', '],
//           subPath: 'nestedArray.1'
//         })).toEqual({
//           main: `${elementMock.firstProp} - ${elementMock.nestedObject.nestedFirst}, ${elementMock.nestedObject.nestedSecond}`,
//           sub: elementMock.nestedArray[1]
//         });
//       });
//     });
//
//     describe('#parseBaseColumn(data: T, column: BaseColumnInterface)', () => {
//       it('should return array of field values based on column', function () {
//         expect((service as any).parseBaseColumn(elementMock, {
//           discriminator: '',
//           paths: 'firstProp'
//         })).toEqual([elementMock.firstProp]);
//
//         expect((service as any).parseBaseColumn(elementMock, {
//           discriminator: '',
//           paths: ['nestedObject.nestedFirst', 'nestedObject.nestedSecond']
//         })).toEqual([elementMock.nestedObject.nestedFirst, elementMock.nestedObject.nestedSecond]);
//
//         expect((service as any).parseBaseColumn(elementMock, {
//           discriminator: '',
//           paths: ['nestedObject.nestedFirst', 'nestedArray.1']
//         })).toEqual([elementMock.nestedObject.nestedFirst, elementMock.nestedArray[1]]);
//       });
//     });
//   });
//
//   describe('(fill defaults tests)', () => {
//     describe('#fillColumnsConfigDefaults(columnsConfig: ColumnsV2Config)', () => {
//     });
//
//     describe('#fillAvatarColumnDefaults(column: AvatarColumnInterface)', () => {
//       it('should complete provided column with some defaults', () => {
//         const columnMock: any = {hello: 'world'};
//
//         expect((service as any).fillAvatarColumnDefaults(columnMock)).toEqual({
//           sortable: false,
//           ...columnMock
//         });
//       });
//     });
//
//     describe('#fillDateColumnDefaults(column: DateColumnInterface)', () => {
//       it('should complete provided column with some defaults', () => {
//         const columnMock: any = {hello: 'world'};
//
//         expect((service as any).fillDateColumnDefaults(columnMock)).toEqual({
//           sortable: false,
//           format: 'yyyy/MM/dd',
//           timeAgo: false,
//           ...columnMock
//         });
//       });
//     });
//
//     describe('#fillIconColumnDefaults(column: IconColumnInterface)', () => {
//       it('should complete provided column with some defaults', () => {
//         const columnMock: any = {hello: 'world'};
//
//         expect((service as any).fillIconColumnDefaults(columnMock)).toEqual({
//           sortable: false,
//           ...columnMock
//         });
//       });
//     });
//
//     describe('#fillProgressBarColumnDefaults(column: ProgressBarColumnInterface)', () => {
//       it('should complete provided column with some defaults', () => {
//         const columnMock: any = {hello: 'world'};
//
//         expect((service as any).fillProgressBarColumnDefaults(columnMock)).toEqual({
//           sortable: false,
//           ...columnMock
//         });
//       });
//     });
//
//     describe('#fillTextColumnDefaults(column: TextColumnInterface)', () => {
//       it('should complete provided column with some defaults', () => {
//         const columnMock: any = {hello: 'world'};
//
//         expect((service as any).fillTextColumnDefaults(columnMock)).toEqual({
//           sortable: false,
//           subPath: null,
//           limit: 30,
//           translatePrefix: '',
//           separator: ' ',
//           mask: undefined,
//           ...columnMock
//         });
//       });
//     });
//
//     describe('#fillBaseColumnDefaults(column: BaseColumnInterface)', () => {
//       it('should complete provided column with some defaults', () => {
//         const columnMock: any = {hello: 'world'};
//
//         expect((service as any).fillBaseColumnDefaults(columnMock)).toEqual({
//           sortable: false,
//           ...columnMock
//         });
//       });
//     });
//   });
// });
