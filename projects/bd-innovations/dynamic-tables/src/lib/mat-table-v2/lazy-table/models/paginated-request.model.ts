import {PaginationConfig} from '../../_shared/configs/pagination.config';

export interface PaginatedRequestModel<T = any> {
  pagination: PaginationConfig;
  sort?: any;
  requestParams?: T;
  textSearch?: string;
}
