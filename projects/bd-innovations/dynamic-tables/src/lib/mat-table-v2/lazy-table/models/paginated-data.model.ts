export interface PaginatedDataModel<T> {
  list: T[];
  totalItems?: number;
}
