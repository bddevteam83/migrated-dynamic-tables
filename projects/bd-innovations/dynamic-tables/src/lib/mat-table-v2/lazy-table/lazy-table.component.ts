import {SelectionModel} from '@angular/cdk/collections';
import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Inject,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  Type,
  ViewChild,
} from '@angular/core';

import {FormControl} from '@angular/forms';
import {MatCheckbox} from '@angular/material/checkbox';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';

import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ControlQuestion, GroupQuestion} from '@bd-innovations/dynamic-form';
import * as _ from 'lodash';
import {debounceTime, distinctUntilChanged, filter} from 'rxjs/operators';

import {TableToken} from '../_shared/abstract/table.token';

import {ActionRequestV2Config} from '../_shared/configs/action-request-v2.config';
import {ActionsV2Config} from '../_shared/configs/actions-v2.config';

import {ColumnsV2Config} from '../_shared/configs/columns-v2.config';
import {ModuleV2Config} from '../_shared/configs/module-v2.config';
import {PaginationConfig} from '../_shared/configs/pagination.config';
import {ParsedObjectConfig} from '../_shared/configs/parsed-object.config';
import {TableInitStatePlaceholderDirective} from '../_shared/directives/table-init-state-placeholder.directive';
import {TableNoResultPlaceholderDirective} from '../_shared/directives/table-no-result-placeholder.directive';
import {ShowTableModeEnum} from '../_shared/enums/show-table-mode.enum';
import {TableSettingsStorageKey} from '../_shared/enums/table-settings-storage.key';
import {EXPANDED_ROW_ANIMATION} from '../_shared/utilities/animations';
import {enumToArray} from '../_shared/utilities/enum-to-array.function';
import {mixinHasSubs} from '../_shared/utilities/has-subs.mixin';
import {ReservedNamesV2} from '../_shared/utilities/reserved-names-v2';
import {EXPANDED_ROW_DATA, TABLE, TABLE_CONFIG} from '../_shared/utilities/table-tokens';
import {ActionsConfigService} from '../actions-config-service/actions-config.service';
import {DisplayedColumnsDialogDataModel} from '../displayed-columns-dialog/displayed-columns-dialog-data.model';
import {DisplayedColumnsDialogComponent} from '../displayed-columns-dialog/displayed-columns-dialog.component';
import {DefaultFiltersDialogComponent} from '../filters-dialog/default-filters-dialog/default-filters-dialog.component';
import {FiltersDialogDataModel} from '../filters-dialog/filters-dialog-data.model';
import {FiltersDialogComponent} from '../filters-dialog/filters-dialog.component';
import {TableHeaderDirective} from '../table-header/table-header.directive';
import {TableService} from '../table.service';

import {PaginatedDataModel} from './models/paginated-data.model';
import {PaginatedRequestModel} from './models/paginated-request.model';
import {StoredSettingsConfig} from '../_shared/configs/stored-settings.config';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'bd-lazy-table',
  templateUrl: './lazy-table.component.html',
  styleUrls: [
    '../_shared/styles/table.component.scss',
    '../../_shared/styles/gradients.scss',
    './lazy-table.component.scss',
  ],
  animations: [
    EXPANDED_ROW_ANIMATION,
  ],
  providers: [
    ActionsConfigService,
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'standard'}},
    {provide: TABLE, useExisting: LazyTableComponent},
  ],
  host: {
    '[class.bd-lazy-table]': 'true',
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
// eslint-disable-next-line @typescript-eslint/ban-types
export class LazyTableComponent<T extends object> extends mixinHasSubs(class {}) implements
  OnChanges,
  OnInit,
  AfterViewInit,
  AfterContentInit,
  OnDestroy,
  TableToken<T> {

  @Input() set actions(actions: ActionsV2Config<T>) {
    this.actionsConfigService.initActions(actions);
  }
  get actions(): ActionsV2Config<T> {
    return this.actionsConfigService.actions;
  }
  @Input() additionalFiltersDialogData: any;
  @Input() bdTableId: string;
  @Input() storedSettingsConfig: StoredSettingsConfig = {
    [TableSettingsStorageKey.FILTERS]: localStorage,
    [TableSettingsStorageKey.COLUMNS]: localStorage,
    [TableSettingsStorageKey.PAGINATION]: null,
    [TableSettingsStorageKey.SORT]: null,
  };
  @Input() set columns(columns: ColumnsV2Config) {
    this._columns = this.service.fillColumnsConfigDefaults(columns);

    this.displayedColumns = Object.entries(this._columns)
      .filter(([key, value]) => !value.hidden)
      .map(([key, value]) => key);
  }
  @Input() customColumnTemplateRefs: {[key: string]: TemplateRef<any>;} = {};

  @Input() expandedComponentRef: Type<any>;
  @Input() filtersDialogConfig: MatDialogConfig = {};
  @Input() filtersDialogRef: Type<FiltersDialogComponent> = DefaultFiltersDialogComponent;
  @Input() filtersValue: any;
  @Input() globalSort = false;
  @Input() openFilterOnInit = false;
  @Input() paginatedData: PaginatedDataModel<T>;
  @Input() pagination: PaginationConfig = {
    pageIndex: 0,
    pageSize: 25,
  };
  @Input() rowClickAction = 'open';
  @Input() sort: Sort;
  @Input() textSearchEnabled = false;
  @Input() textSearchPlaceholder = 'Search';
  @Input() title: string;
  @Input() translatePrefix = '';

  @Output() actionRequest: EventEmitter<ActionRequestV2Config<T>> = new EventEmitter();
  @Output() bulkActionRequest: EventEmitter<ActionRequestV2Config<T[]>> = new EventEmitter();
  @Output() displayedColumnsUpdate: EventEmitter<string[]> = new EventEmitter();
  @Output() filterRemove: EventEmitter<string> = new EventEmitter();
  @Output() paginationRequest: EventEmitter<PaginatedRequestModel> = new EventEmitter();

  @ViewChild('massBulkCheckbox') massBulkCheckbox: MatCheckbox;
  @ViewChild(MatPaginator) matPaginator: MatPaginator;
  @ViewChild(MatSort) matSort: MatSort;

  @ContentChild(TableInitStatePlaceholderDirective) initStatePlaceholder: ElementRef;
  @ContentChild(TableNoResultPlaceholderDirective) noResultPlaceholder: ElementRef;
  @ContentChild(TableHeaderDirective) tableHeader: TableHeaderDirective;

  readonly ShowTableMode = ShowTableModeEnum;

  _columns: ColumnsV2Config;
  bulkSelection: SelectionModel<ParsedObjectConfig<T>> = new SelectionModel(true, []);

  customHeader: boolean;

  dataNotSet = true;
  dataSource: MatTableDataSource<ParsedObjectConfig<T>>;
  displayedColumns: string[];
  expandedComponentInjectors: Injector[];
  expandedElement: ParsedObjectConfig<T>;
  filtersDialogInstance: MatDialogRef<FiltersDialogComponent>;
  filtersMetadata: GroupQuestion;

  hasBulkActions: boolean;
  hasCursorPointer: boolean;
  hasDisplayedColumnsUpdate: boolean;
  hasExpandedRow: boolean;
  hasFilters: boolean;
  hasGlobalSort: boolean;
  hasHeader: boolean;
  hasPagination: boolean;

  reservedNames = ReservedNamesV2;
  showTableMode: ShowTableModeEnum = ShowTableModeEnum.SHOW_TABLE_DATA;
  textSearchControl = new FormControl('');

  constructor(
    @Inject(TABLE_CONFIG) public config: ModuleV2Config,
    private readonly service: TableService<T>,
    readonly actionsConfigService: ActionsConfigService<T>,
    private readonly injector: Injector,
    private readonly dialog: MatDialog,
    private readonly cd: ChangeDetectorRef,
  ) {
    super();
  }

  // LIFECYCLE HOOKS START

  ngOnChanges(changes: SimpleChanges): void {
    // need to exclude first change because setParams() need to be called first (in ngOnInit())

    if (changes.actions && !changes.actions.firstChange) {
      this.actionsConfigService.initFilterBulkActions();
    }

    if (changes.columns && !changes.columns.firstChange) {
      this.addSystemDisplayColumns();
    }

    if (changes.paginatedData) {
      this.bulkSelection.clear();

      if (!changes.paginatedData.firstChange) {
        this.checkDataNotSet();
        this.initDataSource();
        this.matPaginator.length = this.paginatedData.totalItems;
        this.matPaginator.pageIndex = this.pagination.pageIndex;
        this.matPaginator.pageSize = this.pagination.pageSize;

        if (this.bdTableId && this.storedSettingsConfig?.sort) {
          this.sort = this.getTableSettingsFromStorage(this.storedSettingsConfig.sort, TableSettingsStorageKey.SORT);
        }
        if (this.sort) {
          this.matSort.active = this.sort.active;
          this.matSort.direction = this.sort.direction;
          this.dataSource.sort = this.matSort;

          const activeSortHeader = this.matSort.sortables.get(this.sort.active);

          // eslint-disable-next-line no-underscore-dangle
          (activeSortHeader as any)._setAnimationTransitionState?.({
            toState: 'active',
          });
        }
      }
    }
  }

  ngOnInit(): void {
    this.setParams();
    this.initDataSource();
    if (this.hasBulkActions) {
      this.actionsConfigService.initFilterBulkActions();
    }
    if (this.hasFilters) {
      this.initFilters();
    }
    this.openFiltersDialogOnInit();
    this.addSystemDisplayColumns();
  }

  ngAfterContentInit(): void {
    if (this.initStatePlaceholder) {
      this.showTableMode = ShowTableModeEnum.INIT_PLACEHOLDER;
    }
    this.requestTableDataFromSource();
  }

  ngAfterViewInit(): void {
    this.initPagination();

    this.initPaginatedRequestEmitting();

    this.customHeader = !!this.tableHeader;

    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    if (this.filtersDialogInstance) {
      this.filtersDialogInstance.close();
    }
    this.clearSubscriptions();
  }

  emitActionRequest(action: string, index: number): void {
    const req: ActionRequestV2Config<T> = {
      action,
      data: this.paginatedData.list[index],
    };

    this.actionRequest.emit(req);
  }

  emitBulkActionRequest(action: string): void {
    const req: ActionRequestV2Config<T[]> = {
      action,
      data: this.paginatedData.list
        .filter((elem, index) => this.bulkSelection.selected
          .map(parsedElem => parsedElem.originalIndex)
          .includes(index),
        ),
    };

    this.bulkActionRequest.emit(req);
  }

  emitHeaderActionRequest(action: string): void {
    this.actionRequest.emit({action, data: null});
  }

  isAllSelected(): boolean {
    let res = true;

    this.dataSource.data
      .forEach(elem => {
        if (!this.bulkSelection.isSelected(elem)) {
          res = false;
        }
      });

    return res;
  }

  onExpandArrowClicked(element: ParsedObjectConfig<T>): void {
    this.expandedElement = this.expandedElement === element ? null : element;
  }

  onRowClicked(element: ParsedObjectConfig<T>): void {
    if (element.singleActions.includes(this.rowClickAction)) {
      this.emitActionRequest(this.rowClickAction, element.originalIndex);
    }
  }

  openDisplayColumnsDialog(): void {
    const allColumns: string[] = Object.keys(this._columns);
    const currentDisplayColumns: string[] = this.displayedColumns.filter(elem => !enumToArray(ReservedNamesV2).includes(elem));

    const displayedColumnsDialogData: DisplayedColumnsDialogDataModel = {
      translatePrefix: this.translatePrefix,
      displayedColumns: currentDisplayColumns
        .map(elem => ({
          name: elem,
          isDisplayed: true,
        }))
        .concat(allColumns
          .filter(elem => !currentDisplayColumns.includes(elem))
          .map(elem => ({
            name: elem,
            isDisplayed: false,
          }))),
    };

    this.subs$.push(
      this.dialog.open<DisplayedColumnsDialogComponent, DisplayedColumnsDialogDataModel, string[]>(DisplayedColumnsDialogComponent, {
        data: displayedColumnsDialogData,
      }).afterClosed().pipe(
        filter(res => !!res),
      ).subscribe(res => {
        this.setTableSettingsToStorage(this.storedSettingsConfig?.columns, TableSettingsStorageKey.COLUMNS, res);
        this.displayedColumnsUpdate.emit(res);
      }),
    );
  }

  openFiltersDialog(): void {
    this.filtersDialogInstance = this.dialog.open<FiltersDialogComponent, FiltersDialogDataModel, object>(this.filtersDialogRef, {
      ...this.filtersDialogConfig,
      data: {
        initValue: this.filtersValue,
        metadata: this.filtersMetadata,
        additionalData: this.additionalFiltersDialogData,
      },
    });

    this.subs$.push(
      this.filtersDialogInstance
        .afterClosed()
        .pipe(
          filter(res => !!res),
        )
        .subscribe(res => {
          this.setTableSettingsToStorage(this.storedSettingsConfig?.filters, TableSettingsStorageKey.FILTERS, res);
          this.filtersValue = res;

          this.matPaginator.page.emit({
            pageIndex: 0,
            pageSize: this.pagination.pageSize,
            length: this.paginatedData.totalItems,
          });
        }),
    );
  }

  removeFilter(control: string): void {
    this.filtersValue = Object.entries(this.filtersValue).reduce((accumulator, [key, value]) => key === control ? accumulator : {...accumulator, [key]: value}, {});
    this.setTableSettingsToStorage(this.storedSettingsConfig?.filters, TableSettingsStorageKey.FILTERS, this.filtersValue);
    this.matPaginator.page.emit({
      pageIndex: 0,
      pageSize: this.pagination.pageSize,
      length: this.paginatedData.totalItems,
    });
    this.filterRemove.emit(control);
  }

  toggleAll(): void {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    this.isAllSelected() ?
      this.bulkSelection.deselect(...this.dataSource.data) :
      this.bulkSelection.select(...this.dataSource.data);
  }

  private addSystemDisplayColumns(): void {
    if (this.hasExpandedRow) {
      this.displayedColumns.unshift(ReservedNamesV2.expandArrowColumn);
    }
    if (this.hasBulkActions) {
      this.displayedColumns.unshift(ReservedNamesV2.bulkCheckboxColumn);
    }
    if (this.actionsConfigService.hasSingleActions) {
      this.displayedColumns.push(ReservedNamesV2.actionsColumn);
    }
  }

  private checkDataNotSet(): void {
    if (this.paginatedData.totalItems === 0) {
      if (this.noResultPlaceholder) {
        this.showTableMode = ShowTableModeEnum.NO_RESULT_PLACEHOLDER;
      } else {
        this.showTableMode = ShowTableModeEnum.SHOW_TABLE_DATA;
      }
    } else {
      this.dataNotSet = false;
      this.showTableMode = ShowTableModeEnum.SHOW_TABLE_DATA;
    }
  }

  private getTableSettingsFromStorage(storageProvider: Storage, storageKeyName: TableSettingsStorageKey): any {
    try {
      return JSON.parse(storageProvider.getItem(`${this.bdTableId}${storageKeyName}`));
    } catch (error) {
      throw new Error(`Bd Lazy Table settings of ${storageKeyName} has invalid value or have been corrupted`);
    }
  }

  private initDataSource(): void {
    this.dataSource = new MatTableDataSource<ParsedObjectConfig<T>>(
      this.service.parseData(
        this.paginatedData.list,
        this._columns,
        this.actionsConfigService.actions,
      ),
    );

    if (this.hasExpandedRow) {
      this.initExpandedComponentInjectors();
    }
  }

  private initExpandedComponentInjectors(): void {
    this.expandedComponentInjectors = this.paginatedData.list.map(elem => Injector.create({
      providers: [
        {provide: EXPANDED_ROW_DATA, useValue: elem},
      ],
      parent: this.injector,
    }));
  }

  private initFilters(): void {
    this.filtersMetadata = {
      discriminator: 'group',
      direction: 'row',
      controls: _.mapValues(this._columns, column => ({
        discriminator: 'control',
        showTitle: true,
        control: column.control,
      } as ControlQuestion)),
    } as GroupQuestion;

    Object.keys(this.filtersMetadata.controls)
      .forEach(key => {
        if (!(this.filtersMetadata.controls[key] as ControlQuestion).control) {
          delete this.filtersMetadata.controls[key];
        }
      });
  }

  private initPaginatedRequestEmitting(): void {
    this.subs$.push(
      this.matPaginator.page
        .subscribe(({pageIndex, pageSize}: PageEvent) => {
          const sort: Sort = this.matSort.active && this.matSort.direction !== '' ? {
            active: this.matSort.active,
            direction: this.matSort.direction,
          } : null;
          const pagination: PaginationConfig = {pageIndex, pageSize};
          const textSearch: string = this.textSearchControl.value ? this.textSearchControl.value : null;

          this.setTableSettingsToStorage(
            this.storedSettingsConfig?.pagination,
            TableSettingsStorageKey.PAGINATION,
            pagination,
          );
          this.paginationRequest.emit({pagination, sort, requestParams: this.filtersValue, textSearch});
        }),
      this.matSort.sortChange
        .subscribe(({active, direction}: Sort) => {
          const sort: Sort = active && direction !== '' ? {active, direction} : null;
          const pagination: PaginationConfig = {pageIndex: this.matPaginator.pageIndex, pageSize: this.matPaginator.pageSize};
          const textSearch: string = this.textSearchControl.value ? this.textSearchControl.value : null;

          this.setTableSettingsToStorage(this.storedSettingsConfig?.sort, TableSettingsStorageKey.SORT, sort);
          this.paginationRequest.emit({pagination, sort, requestParams: this.filtersValue, textSearch});
        }),
      this.textSearchControl.valueChanges.pipe(
        distinctUntilChanged(),
        debounceTime(500),
      ).subscribe(() => {
        this.matPaginator.page.emit({
          pageIndex: 0,
          pageSize: this.pagination.pageSize,
          length: this.paginatedData.totalItems,
        });
      }),
    );
  }

  private initPagination(): void {
    this.matPaginator.pageIndex = this.pagination.pageIndex;
    this.matPaginator.pageSize = this.pagination.pageSize;
    this.matPaginator.pageSizeOptions = this.config.paginatorPageSizeOptions;
    this.matPaginator.length = this.paginatedData.totalItems;

    if (this.sort) {
      this.matSort.active = this.sort.active;
      this.matSort.direction = this.sort.direction;
      this.dataSource.sort = this.matSort;

      const activeSortHeader = this.matSort.sortables.get(this.sort.active);

      // eslint-disable-next-line no-underscore-dangle
      (activeSortHeader as any)._setAnimationTransitionState?.({
        toState: 'active',
      });
    }
  }

  private openFiltersDialogOnInit(): void {
    if (this.openFilterOnInit) {
      this.openFiltersDialog();
      this.openFilterOnInit = false;
    }
  }

  private requestTableDataFromSource(): void {
    if (this.service.hasAnyActiveFilter(this.filtersValue) || !this.initStatePlaceholder) {
      this.paginationRequest.emit({requestParams: this.filtersValue, pagination: this.pagination, sort: this.sort});
    }
  }

  private setParams(): void {
    if (this.bdTableId) {
      if (this.storedSettingsConfig?.filters) {
        this.filtersValue = this.getTableSettingsFromStorage(this.storedSettingsConfig.filters, TableSettingsStorageKey.FILTERS);
      }
      this.pagination = this.storedSettingsConfig?.pagination
        ? this.getTableSettingsFromStorage(this.storedSettingsConfig.pagination, TableSettingsStorageKey.PAGINATION)
        : this.pagination;

      if (this.storedSettingsConfig?.sort) {
        this.sort = this.getTableSettingsFromStorage(this.storedSettingsConfig.sort, TableSettingsStorageKey.SORT);
      }

      if (this.storedSettingsConfig?.columns) {
        const columnsConfig = this.getTableSettingsFromStorage(this.storedSettingsConfig.columns, TableSettingsStorageKey.COLUMNS);

        if (columnsConfig) {
          this.displayedColumnsUpdate.emit(columnsConfig);
        }
      }
    }
    this.actionsConfigService.checkActions(!!this.actionRequest.observers.length);
    this.hasBulkActions = this.actionsConfigService.hasBulkActions;
    this.hasPagination = !!this.pagination;
    this.hasGlobalSort = !!this.globalSort;
    this.hasExpandedRow = !!this.expandedComponentRef;
    this.hasFilters = !!_.map(this._columns, 'control').filter(elem => elem).length;
    this.hasDisplayedColumnsUpdate = !!this.displayedColumnsUpdate.observers.length;
    this.hasCursorPointer = !!(this.actionsConfigService.actions[this.rowClickAction] && this.actionsConfigService.hasSingleActions);
    this.hasHeader = !!this.title || this.hasBulkActions || this.hasFilters ||
      this.hasDisplayedColumnsUpdate || this.actionsConfigService.hasHeaderButtons || this.textSearchEnabled;
  }

  private setTableSettingsToStorage(storageProvider: Storage | null, settingsKeyName: TableSettingsStorageKey, value: any): void {
    if (this.bdTableId && this.storedSettingsConfig && storageProvider) {
      storageProvider.setItem(`${this.bdTableId}${settingsKeyName}`, JSON.stringify(value));
    }
  }
}
