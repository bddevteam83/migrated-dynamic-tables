// import {ComponentFixture, TestBed} from '@angular/core/testing';
// import {LazyTableComponent} from './lazy-table.component';
// import {CommonModule} from '@angular/common';
// import {
//   MatCell,
//   MatCellDef,
//   MatColumnDef,
//   MatFooterCell,
//   MatFooterCellDef,
//   MatFooterRow,
//   MatFooterRowDef,
//   MatHeaderCell,
//   MatHeaderCellDef,
//   MatHeaderRow,
//   MatHeaderRowDef,
//   MatRow,
//   MatRowDef,
//   MatTable
// } from '@angular/material/table';
// import {MockComponent, MockDirective, MockPipe} from 'ng-mocks';
// import {
//   MatButton,
//   MatCheckbox,
//   MatIcon,
//   MatMenu,
//   MatMenuTrigger,
//   MatPaginator,
//   MatProgressBar,
//   MatSort,
//   MatSortHeader,
//   MatTooltip
// } from '@angular/material';
// import {TranslatePipe} from '@ngx-translate/core';
// import {
//   BdDatePipe,
//   FirstLetterPipe,
//   NullOrUndefinedHandlerPipe,
//   OrderedKeyValuePipe,
//   RandomGradientPipe,
//   SliceWithDotsPipe,
//   TimeAgoPipe,
//   ToIconPipe
// } from '@bd-innovations/pipe-collection';
// import {RandomClassDirective, ShortPressDirective} from '@bd-innovations/directive-collection';
// import {FormsModule} from '@angular/forms';
// import {TableService} from '../table.service';
// import {TABLE_CONFIG} from '../_shared/utilities/table-tokens';
// import {ReservedNamesEnum} from '../../mat-dynamic-table/models/reserved-names.enum';
// import {Subject} from 'rxjs';
// import {EventEmitter} from '@angular/core';
//
// describe('LazyTableComponent', () => {
//   let component: LazyTableComponent<any>;
//   let fixture: ComponentFixture<LazyTableComponent<any>>;
//
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         CommonModule,
//         FormsModule,
//       ],
//       declarations: [
//         LazyTableComponent,
//
//         MockComponent(MatTable),
//         MockComponent(MatPaginator),
//         MockComponent(MatButton),
//         MockComponent(MatMenu),
//         MockComponent(MatIcon),
//         MockComponent(MatCheckbox),
//         MockComponent(MatProgressBar),
//         MockComponent(MatSortHeader),
//
//         MockPipe(TranslatePipe),
//
//         MockPipe(TimeAgoPipe),
//         MockPipe(RandomGradientPipe),
//         MockPipe(NullOrUndefinedHandlerPipe),
//         MockPipe(SliceWithDotsPipe),
//         MockPipe(ToIconPipe),
//         MockPipe(BdDatePipe),
//         MockPipe(OrderedKeyValuePipe),
//         MockPipe(FirstLetterPipe),
//
//         MockDirective(MatMenuTrigger),
//         MockDirective(MatSort),
//         MockDirective(MatTooltip),
//         MockDirective(MatHeaderCellDef),
//         MockDirective(MatHeaderRowDef),
//         MockDirective(MatColumnDef),
//         MockDirective(MatCellDef),
//         MockDirective(MatRowDef),
//         MockDirective(MatFooterCellDef),
//         MockDirective(MatFooterRowDef),
//         MockDirective(MatHeaderCell),
//         MockDirective(MatCell),
//         MockDirective(MatFooterCell),
//         MockDirective(MatHeaderRow),
//         MockDirective(MatRow),
//         MockDirective(MatFooterRow),
//
//         MockDirective(ShortPressDirective),
//         MockDirective(RandomClassDirective),
//       ],
//       providers: [
//         {
//           provide: TableService,
//           useValue: {
//             fillColumnsConfigDefaults: () => {
//             }
//           }
//         },
//         {provide: TABLE_CONFIG, useValue: {}}
//       ]
//     }).compileComponents();
//
//     fixture = TestBed.createComponent(LazyTableComponent);
//     component = fixture.componentInstance;
//   });
//
//   it('should be created', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should have some default values', () => {
//     expect(component.paginatedData).toBeFalsy();
//     expect(component.columns).toEqual({});
//
//     expect(component.bulkActions).toBeFalsy();
//     expect(component.actions).toEqual([]);
//     expect(component.conditionalActions).toEqual([]);
//     expect(component.userRoles).toBeFalsy();
//     expect(component.pagination).toEqual({
//       pageIndex: 0,
//       pageSize: 25
//     });
//
//     expect(component.translatePrefix).toEqual('');
//     expect(component.expandedComponentRef).toBeFalsy();
//
//     expect(component.dataSource).toBeFalsy();
//     expect(component.expandedElement).toBeFalsy();
//     expect(component.expandedComponentInjectors).toBeFalsy();
//     expect(component.reservedNames).toEqual(ReservedNamesEnum);
//     expect(component.hasCursorPointer).toBeFalsy();
//     expect(component.displayedColumns).toEqual([]);
//     expect(component.bulkContainer$).toEqual(new Subject());
//
//     // TODO test these defaults (because of ng-mocks them fail)
//     //  expect(component.matPaginator instanceof MatPaginator).toBeTruthy();
//     //  expect(component.matSort instanceof MatSort).toBeTruthy();
//     //  expect(component.massBulkCheckbox instanceof MatCheckbox).toBeTruthy();
//     //  expect(component.matPaginator).toBeTruthy();
//     //  expect(component.matSort).toBeTruthy();
//     //  expect(component.massBulkCheckbox).toBeTruthy();
//
//     expect(component.actionRequest instanceof EventEmitter).toBeTruthy();
//     expect(component.actionRequest).toBeTruthy();
//     expect(component.paginationRequest instanceof EventEmitter).toBeTruthy();
//     expect(component.paginationRequest).toBeTruthy();
//     expect(component.paginatedBulkActionRequest instanceof EventEmitter).toBeTruthy();
//     expect(component.paginatedBulkActionRequest).toBeTruthy();
//   });
//
//   describe('#ngOnChanges(changes: SimpleChanges)', () => {
//     it('should call #setIsCursorPointer(changes: SimpleChanges) if changes.actions is truthy', function () {
//       const changesMock: any = {actions: {currentValue: true}};
//       const setIsCursorPointerSpy = spyOn(component as any, 'setIsCursorPointer');
//
//       component.ngOnChanges(changesMock);
//
//       expect(setIsCursorPointerSpy).toHaveBeenCalledTimes(1);
//     });
//
//     it('shouldn\'t call #setIsCursorPointer(changes: SimpleChanges) if changes.actions is falsy', function () {
//       const changesMock: any = {actions: {currentValue: null}};
//       const setIsCursorPointerSpy = spyOn(component as any, 'setIsCursorPointer');
//
//       component.ngOnChanges(changesMock);
//
//       expect(setIsCursorPointerSpy).not.toHaveBeenCalled();
//     });
//
//     it('should call service #fillColumnsConfigDefaults(columnsConfig: ColumnsV2Config) and pass there changes if changes.columns is truthy', () => {
//       const changesMock: any = {columns: {currentValue: 'hello world'}};
//       const resMock: any = 'result';
//
//       const fillColumnsConfigDefaultsSpy = spyOn(TestBed.inject(TableService), 'fillColumnsConfigDefaults')
//         .and.callFake(args => {
//           expect(args).toEqual(changesMock.columns.currentValue);
//
//           return resMock;
//         });
//
//       component.ngOnChanges(changesMock);
//
//       expect(fillColumnsConfigDefaultsSpy).toHaveBeenCalledTimes(1);
//       expect(component.columns).toEqual(resMock);
//     });
//
//     it('shouldn\'t call service #fillColumnsConfigDefaults(columnsConfig: ColumnsV2Config) and pass there changes if changes.actions is falsy', function () {
//       const changesMock: any = {columns: {currentValue: null}};
//
//       const fillColumnsConfigDefaultsSpy = spyOn(TestBed.inject(TableService), 'fillColumnsConfigDefaults');
//
//       component.ngOnChanges(changesMock);
//
//       expect(fillColumnsConfigDefaultsSpy).not.toHaveBeenCalled();
//     });
//
//     it('should call #initPagination(), #initDataSource() if changes.paginatedData is truthy and #initExpandedComponentInjectors() if also expandedComponentRef is truthy', () => {
//       const changesMock: any = {paginatedData: {currentValue: 'hello world'}};
//       component.expandedComponentRef = true as any;
//
//       const initPaginationSpy = spyOn(component as any, 'initPagination');
//       const initDataSourceSpy = spyOn(component as any, 'initDataSource');
//       const initExpandedComponentInjectorsSpy = spyOn(component as any, 'initExpandedComponentInjectors');
//
//       component.ngOnChanges(changesMock);
//
//       expect(initPaginationSpy).toHaveBeenCalledTimes(1);
//       expect(initDataSourceSpy).toHaveBeenCalledTimes(1);
//       expect(initExpandedComponentInjectorsSpy).toHaveBeenCalledTimes(1);
//     });
//
//     it('shouldn\'t call #initPagination(), #initDataSource() if changes.paginatedData is falsy and neither #initExpandedComponentInjectors() if also expandedComponentRef is falsy', function () {
//       const changesMock: any = {paginatedData: {currentValue: null}};
//
//       const initPaginationSpy = spyOn(component as any, 'initPagination');
//       const initDataSourceSpy = spyOn(component as any, 'initDataSource');
//       const initExpandedComponentInjectorsSpy = spyOn(component as any, 'initExpandedComponentInjectors');
//
//       component.ngOnChanges(changesMock);
//
//       expect(initPaginationSpy).not.toHaveBeenCalled();
//       expect(initDataSourceSpy).not.toHaveBeenCalled();
//       expect(initExpandedComponentInjectorsSpy).not.toHaveBeenCalled();
//     });
//   });
//
//   describe('#ngOnInit()', () => {
//   });
//
//   describe('#ngAfterViewInit()', () => {
//   });
//
//   describe('#ngOnDestroy()', () => {
//   });
//
//   describe('#isFirstColumnPadding(column_name: string)', () => {
//   });
//
//   describe('#emitActionRequest(actionType: string, index: number)', () => {
//   });
//
//   describe('#massSelectionChange()', () => {
//   });
//
//   describe('#onRowClicked(element: ParsedObjectConfig)', () => {
//   });
//
//   describe('#onExpandedClicked(element: ParsedObjectConfig)', () => {
//   });
//
//   describe('#initDataSource()', () => {
//   });
//
//   describe('#initPagination()', () => {
//   });
//
//   describe('#initBulk()', () => {
//   });
//
//   describe('#initExpandedComponentInjectors()', () => {
//   });
//
//   describe('#setIsCursorPointer()', () => {
//   });
//
//   describe('#initPaginatedRequestEmitting()', () => {
//   });
//
// });
