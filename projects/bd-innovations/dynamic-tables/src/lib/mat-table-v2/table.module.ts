import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableComponent} from './table/table.component';
import {TableService} from './table.service';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {ClickerDirectiveModule, DomElementValidatorDirectiveModule, RandomClassDirectiveModule} from '@bd-innovations/directive-collection';
import {TranslateModule} from '@ngx-translate/core';
import {MatButtonModule} from '@angular/material/button';
import {
  BdDatePipeModule,
  FirstLetterPipeModule,
  NullOrUndefinedHandlerPipeModule,
  OrderedKeyValuePipeModule,
  RandomGradientPipeModule,
  SliceWithDotsPipeModule,
  TimeAgoPipeModule,
} from '@bd-innovations/pipe-collection';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {LazyTableComponent} from './lazy-table/lazy-table.component';
import {TABLE_CONFIG} from './_shared/utilities/table-tokens';
import {ModuleV2Config} from './_shared/configs/module-v2.config';
import {DefaultFiltersDialogComponent} from './filters-dialog/default-filters-dialog/default-filters-dialog.component';
import {DisplayedColumnsDialogComponent} from './displayed-columns-dialog/displayed-columns-dialog.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {FlexLayoutModule} from '@angular/flex-layout';
import {DynamicFormModule} from '@bd-innovations/dynamic-form';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {ToIconModule} from './_shared/pipes/to-icon/to-icon.module';
import {ToLabelModule} from './_shared/pipes/to-label/to-label.module';
import {BdPaginatorIntl} from './_shared/customisation/bd-paginator.intl';
import {TextColumnCellComponent} from './_shared/components/data-column/text-column-cell/text-column-cell.component';
import {AvatarColumnCellComponent} from './_shared/components/data-column/avatar-column-cell/avatar-column-cell.component';
import {DateColumnCellComponent} from './_shared/components/data-column/date-column-cell/date-column-cell.component';
import {IconColumnCellComponent} from './_shared/components/data-column/icon-column-cell/icon-column-cell.component';
import {ProgressBarColumnCellComponent} from './_shared/components/data-column/progress-bar-column-cell/progress-bar-column-cell.component';
import {ActiveFiltersModule} from './_shared/pipes/active-filters/active-filters.module';
import {ActiveFiltersPipe} from './_shared/pipes/active-filters/active-filters.pipe';
import {TableHeaderModule} from './table-header/table-header.module';

@NgModule({
  declarations: [
    TableComponent,
    LazyTableComponent,
    DefaultFiltersDialogComponent,
    DisplayedColumnsDialogComponent,
    TextColumnCellComponent,
    AvatarColumnCellComponent,
    DateColumnCellComponent,
    IconColumnCellComponent,
    ProgressBarColumnCellComponent,
  ],
  entryComponents: [
    DefaultFiltersDialogComponent,
    DisplayedColumnsDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DragDropModule,
    FlexLayoutModule,

    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatDialogModule,
    MatCardModule,

    TranslateModule,

    TimeAgoPipeModule,
    RandomGradientPipeModule,
    NullOrUndefinedHandlerPipeModule,
    SliceWithDotsPipeModule,
    ToIconModule,
    BdDatePipeModule,
    OrderedKeyValuePipeModule,
    FirstLetterPipeModule,

    RandomClassDirectiveModule,
    DynamicFormModule,
    MatChipsModule,
    ToLabelModule,
    ClickerDirectiveModule,
    DomElementValidatorDirectiveModule,
    TableHeaderModule,
    ReactiveFormsModule,
    ActiveFiltersModule,
  ],
  exports: [
    TableComponent,
    LazyTableComponent
  ]
})
export class TableModule {

  static forRoot(moduleConfig: ModuleV2Config): ModuleWithProviders<TableModule> {
    return {
      ngModule: TableModule,
      providers: [
        TableService,
        ActiveFiltersPipe,
        {provide: TABLE_CONFIG, useValue: moduleConfig},
        {provide: MatPaginatorIntl, useClass: BdPaginatorIntl}
      ]
    };
  }

}
