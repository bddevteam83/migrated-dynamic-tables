export interface ApiRequestModel {
  search: string;
  paginator: PaginationRequestModel;
  sort: SortRequestModel;
}

export interface PaginationRequestModel {
  pageIndex: number;
  pageSize: number;
  length?: number;
}

export interface SortRequestModel {
  active: string;
  direction: UpperCaseSortDirection;
}

export type UpperCaseSortDirection = 'ASC' | 'DESC' | null;

export interface PaginatedResponseModel<T> {
  list: T[];
  totalItems: number;
}
