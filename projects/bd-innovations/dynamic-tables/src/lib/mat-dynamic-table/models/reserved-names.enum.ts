export enum ReservedNamesEnum {
  searchRow = 'bdReservedNameForSearchRow',
  expandedRow = 'bdReservedNameForExpandedRow',
  // expandColumn = 'bdReservedNameForExpandColumn',
  actionsColumn = 'bdReservedNameForActionsColumn',
  // checkboxColumn = 'bdReservedNameForCheckboxColumn',
  serviceColumn = 'bdReservedNameForServiceColumn'
}
