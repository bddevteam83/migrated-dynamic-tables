export interface ActionRequestModel<T> {
  type: string;
  entity: T;
}
