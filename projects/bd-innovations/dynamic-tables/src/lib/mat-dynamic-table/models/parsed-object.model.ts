export interface ParsedObjectModel<T> {
  originalIndex: number;
  selected: boolean;

  [key: string]: any;
}
