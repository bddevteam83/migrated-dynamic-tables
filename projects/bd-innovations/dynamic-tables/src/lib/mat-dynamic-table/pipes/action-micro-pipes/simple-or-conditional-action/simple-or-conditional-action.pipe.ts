import {Pipe, PipeTransform} from '@angular/core';
import {ConditionalActionConfig} from '../../../configs/conditional-action.config';

@Pipe({
  name: 'simpleOrConditionalAction'
})
export class SimpleOrConditionalActionPipe implements PipeTransform {

  transform(value: string | ConditionalActionConfig<any>): 'simple' | 'conditional' {
    switch (true) {
      case (typeof value === 'string'):
        return 'simple';
      case (value instanceof Object):
        return 'conditional';
      default:
        throw new Error('Unknown Action type passed');

    }
  }

}
