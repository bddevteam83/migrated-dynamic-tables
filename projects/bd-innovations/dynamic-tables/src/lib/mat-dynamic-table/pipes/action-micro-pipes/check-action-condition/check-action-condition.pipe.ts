import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'checkActionCondition'
})
export class CheckActionConditionPipe<T> implements PipeTransform {

  transform(element: T, callback: (element: T) => boolean): boolean {
    return callback(element);
  }

}
