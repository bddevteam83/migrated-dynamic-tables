import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDynamicTableComponent} from './mat-dynamic-table.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {
  BdDatePipeModule,
  FirstLetterPipeModule,
  NullOrUndefinedHandlerPipeModule,
  OrderedKeyValuePipeModule,
  RandomGradientPipeModule,
  SliceWithDotsPipeModule,
  TimeAgoPipeModule,
  ToIconPipeModule
} from '@bd-innovations/pipe-collection';
import {
  ApiDataHandlerDirectiveModule,
  DomElementValidatorDirectiveModule,
  RandomClassDirectiveModule,
  ShortPressDirectiveModule
} from '@bd-innovations/directive-collection';
import {ModuleConfig} from './configs/module.config';
import {DYNAMIC_TABLE_MODULE_CONFIG, MatDynamicTableService} from './mat-dynamic-table.service';
import {ActionMicroPipesModule} from './pipes/action-micro-pipes/action-micro-pipes.module';
import {FlexModule} from '@angular/flex-layout';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  declarations: [
    MatDynamicTableComponent
  ],
  imports: [
    CommonModule,
    FlexModule,
    FormsModule,
    ReactiveFormsModule,

    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCheckboxModule,

    TranslateModule,

    TimeAgoPipeModule,
    RandomGradientPipeModule,
    NullOrUndefinedHandlerPipeModule,
    SliceWithDotsPipeModule,
    ToIconPipeModule,
    TimeAgoPipeModule,
    BdDatePipeModule,
    OrderedKeyValuePipeModule,
    FirstLetterPipeModule,

    DomElementValidatorDirectiveModule,
    ShortPressDirectiveModule,
    RandomClassDirectiveModule,
    ApiDataHandlerDirectiveModule,

    ActionMicroPipesModule
  ],
  exports: [
    MatDynamicTableComponent,
  ]
})
export class MatDynamicTableModule {

  static forRoot(moduleConfig: ModuleConfig): ModuleWithProviders<MatDynamicTableModule> {
    return {
      ngModule: MatDynamicTableModule,
      providers: [
        MatDynamicTableService,
        {
          provide: DYNAMIC_TABLE_MODULE_CONFIG,
          useValue: moduleConfig
        }
      ]
    };
  }

  static forChild(moduleConfig: ModuleConfig): ModuleWithProviders<MatDynamicTableModule> {
    return {
      ngModule: MatDynamicTableModule,
      providers: [
        MatDynamicTableService,
        {
          provide: DYNAMIC_TABLE_MODULE_CONFIG,
          useValue: moduleConfig
        }
      ]
    };
  }

}
