import {ConditionalActionConfig} from './conditional-action.config';

export type ActionsConfig<T> = Array<string | ConditionalActionConfig<T>>;

