export interface ConditionalActionConfig<T> {
  type: string;
  condition?: (element: T) => boolean;
}
