import {ActionButtonConfig} from './action-button.config';

export interface GlobalActionsConfig {
  icon: string;
  actions: { [key: string]: ActionButtonConfig };
}
