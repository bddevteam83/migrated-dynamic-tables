import {GlobalActionsConfig} from './global-actions.config';

export interface ModuleConfig {
  searchField: SearchFieldConfig;
  actionsColumn: GlobalActionsConfig;
  expandIcon: string;
}

export interface SearchFieldConfig {
  icon: string;
  placeholder: string;
  resetIcon: string;
}

/**
 * @deprecated use ModuleConfig instead
 */
export type MatDynamicTableModuleConfig = ModuleConfig;
