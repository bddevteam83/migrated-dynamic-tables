import {BaseColumn} from '../columns/base.column';

export interface ColumnsConfig {
  [key: string]: BaseColumn;
}
