export interface ActionButtonConfig {
  icon: string;
  label: string;
}
