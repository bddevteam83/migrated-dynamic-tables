export interface RolesConfig {
  [key: string]: string;
}
