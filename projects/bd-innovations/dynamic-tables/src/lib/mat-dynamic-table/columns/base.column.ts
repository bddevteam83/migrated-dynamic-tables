import * as _ from 'lodash';

export abstract class BaseColumn {
  abstract _columnType: string;

  /**
   * @param paths declare property/ies of object to display (using the same syntax of "_.at" method)
   * @param mask declare some mask to use for displayed data
   * @param config ''
   */
  protected constructor(public paths: string | string[],
                        // public mask?: (element: any) => any,
                        public config: BaseColumnConfig = {sortable: false}) {
    // if (config && config.sortable) {
    //   console.log(123)
    // } else {
    //   console.log(456)
    // }
  }

  get(element: any): any {
    return _.at(element, this.paths);
  }
}


export interface BaseColumnConfig {
  sortable?: boolean;
}
