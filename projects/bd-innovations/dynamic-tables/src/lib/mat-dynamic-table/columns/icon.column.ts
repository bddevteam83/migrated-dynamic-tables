import {BaseColumn, BaseColumnConfig} from './base.column';
import {Icon} from '@bd-innovations/pipe-collection';

export class IconColumn extends BaseColumn {
  _columnType = 'IconColumn';
  icons: Icon[];
  action: string;

  // /**
  //  * @param path see BaseColumnInterface.paths
   // * @param icons specify an array of icons from where to take icons based on value of object property
   // * @param config ''
   // */
  constructor(public path: string, config: IconColumnConfig) {
    super(path);
    this.icons = config.icons;
    this.action = config.action;
  }

  get(element: any): string {
    const res: any = super.get(element);
    if (res === null || undefined) {
      return null;
    }

    return res[0];
  }
}


export interface IconColumnConfig extends BaseColumnConfig {
  icons: Icon[];
  action?: string;
}
