import {TextColumn} from './text.column';
import * as _ from 'lodash';
import {BaseColumnConfig} from './base.column';

// TODO add primaryKey property
export class UuidColumn extends TextColumn {
  _columnType = 'TextColumn';
  public array: any[];
  public pathInArray: string | string[];
  public primaryKeyInArray;
  public limit: number;

  constructor(uuId: string, config: UuidColumnConfig) {
    super(uuId, {limit: config.limit});

    this.array = config.array;
    this.pathInArray = config.pathInArray;
    this.primaryKeyInArray = config.primaryKeyInArray;
  }

  get(element: any): string {
    const res: any = super.get(element);
    if (!res) {
      return null;
    }

    const externalRes = _.at(
      this.array.find(elem => elem[this.primaryKeyInArray].toString() === res.toString()),
      this.pathInArray
    );
    if (!externalRes) {
      return null;
    }

    return externalRes.join(' ');
  }
}

export interface UuidColumnConfig extends BaseColumnConfig {
  array: any[];
  pathInArray: string | string[];
  primaryKeyInArray: string;
  limit?: number;
}
