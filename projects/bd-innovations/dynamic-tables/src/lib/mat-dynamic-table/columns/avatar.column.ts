import {BaseColumn, BaseColumnConfig} from './base.column';

export class AvatarColumn extends BaseColumn {
  _columnType = 'AvatarColumn';

  constructor( paths: string | string[] ) {
    super(paths);
  }

  get(element: any): string {
    const res: any = super.get(element);
    if (res === null || undefined) {
      return null;
    }
    return res[0];
  }
}

