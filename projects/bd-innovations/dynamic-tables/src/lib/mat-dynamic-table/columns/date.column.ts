import {BaseColumn, BaseColumnConfig} from './base.column';

export class DateColumn extends BaseColumn {
  _columnType = 'DateColumn';
  format: string;
  timeAgo: boolean;

  // /**
  //  * @param path see BaseColumnInterface.paths
  //  * @param format define format in which to show date (by default equal to 'yyyy/MM/dd'
  //  */
  // constructor(path: string, format?: string);
  // /**
  //  * @param path see BaseColumnInterface.paths
  //  * @param timeAgo set to true when you need to show date in style of "5 minutes ago" (by default equal to false)
  //  */
  // constructor(path: string, timeAgo: boolean);
  constructor(public path, config?: DateColumnConfig) {
    super(path);

    const defaultFormat = 'yyyy/MM/dd';
    const defaultTimeAgo = false;

    this.format = config && config.format || defaultFormat;
    this.timeAgo = config && config.timeAgo || defaultTimeAgo;
  }

  get(element: any): any {
    const res = super.get(element);
    if (res === null || undefined) {
      return null;
    }

    return res[0];
  }
}

export interface DateColumnConfig extends BaseColumnConfig {
  format?: string;
  timeAgo?: boolean;
}
