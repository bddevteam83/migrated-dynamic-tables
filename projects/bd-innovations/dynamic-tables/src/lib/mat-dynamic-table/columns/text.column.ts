import {BaseColumn, BaseColumnConfig} from './base.column';

export class TextColumn extends BaseColumn {
  _columnType = 'TextColumn';
  limit: number;
  translate: string;
  separator: string | string[];
  mask: Mask;

  constructor(paths: string | string[], config?: TextColumnConfig) {
    super(paths, config);

    const defaultLimit = 30;
    const defaultTranslate = '';
    const defaultSeparator = ' ';

    this.limit = config && config.limit || defaultLimit;
    this.translate = config && config.translate || defaultTranslate;
    this.separator = config && config.separator || defaultSeparator;
    this.mask = config && config.mask || undefined;
  }

  get(element: any): string {
    let res: any = super.get(element);
    res = !!this.mask ? res.map(elem => this.mask(elem)) : res;

    if (res === null || res === undefined) {
      return null;
    }
    if (res instanceof Array) {
      let checkRes = false;
      res.forEach((resElem, index) => {
        if (resElem !== null && resElem !== undefined) {
          checkRes = true;
        } else {
          res[index] = '';
        }
      });
      if (!checkRes) {
        return null;
      }
    }

    return res && (res
      .map((elem, index) => ((this.separator as any) instanceof Array ? this.separator[index] : '') + this.translate + elem)
      .join((this.separator as any) instanceof Array ? '' : this.separator
      ));
  }
}


export interface TextColumnConfig extends BaseColumnConfig {
  limit?: number;
  translate?: string;
  separator?: string | string[];
  mask?: Mask;
}

export type Mask = (element: any) => any;
