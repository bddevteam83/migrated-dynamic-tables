import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {FormControl} from '@angular/forms';
import {ApiRequestModel, PaginationRequestModel, SortRequestModel, UpperCaseSortDirection} from './models/api-request.model';
import {fromEvent, Observable, Subject} from 'rxjs';
import {MatDynamicTableService} from './mat-dynamic-table.service';
import {debounceTime, distinctUntilChanged, filter, map, startWith, takeUntil} from 'rxjs/operators';
import {combineLatest} from 'rxjs/internal/observable/combineLatest';
import {ParsedObjectModel} from './models/parsed-object.model';
import {ReservedNamesEnum} from './models/reserved-names.enum';
import {ColumnsConfig} from './configs/columns.config';
import {ActionsConfig} from './configs/actions.config';
import {RolesConfig} from './configs/roles.config';
import {ActionRequestModel} from './models/action-request.model';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {MatCheckbox} from '@angular/material/checkbox';

@Component({
  selector: 'bd-mat-dynamic-table',
  templateUrl: './mat-dynamic-table.component.html',
  styleUrls: ['./mat-dynamic-table.component.scss', '../_shared/styles/gradients.scss', './styles/helper.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MatDynamicTableComponent<T> implements OnChanges, OnInit, AfterViewInit, OnDestroy {

  @Input() data: T[];

  @Input() columns: ColumnsConfig = {};
  @Input() displayColumns: string[] = [];
  @Input() translatePath = '';

  @Input() apiable: boolean;
  @Input() bulk: boolean;

  @Input() search = true;
  @Input() paginator: PaginationRequestModel = {
    pageIndex: 0,
    pageSize: 25,
  };

  @Input() actions: ActionsConfig<T> = [];
  @Input() roles: RolesConfig = {};

  @Input() expandedRow: TemplateRef<any>;

  dataSource: MatTableDataSource<ParsedObjectModel<T>>;
  expandedElement: ParsedObjectModel<T>;
  reservedColumnNames = ReservedNamesEnum;  // Shortcut to access from the template

  searchCtrl: FormControl = new FormControl();
  previousSearch: string;
  @ViewChild('searchInput') searchInput: ElementRef;
  @ViewChild(MatPaginator) matPaginator: MatPaginator;
  @ViewChild(MatSort) matSort: MatSort;

  bulkContainer$: Subject<T[]> = new Subject();
  @ViewChild('massBulkCheckbox') massBulkCheckbox: MatCheckbox;

  dataColumns = [];

  @Output() actionRequest: EventEmitter<ActionRequestModel<T>> = new EventEmitter();
  @Output() apiRequest: EventEmitter<ApiRequestModel> = new EventEmitter();
  @Output() bulkContainerUpdate: EventEmitter<T[]> = new EventEmitter();

  protected onDestroy: Subject<void> = new Subject();

  constructor(public service: MatDynamicTableService<T>,
              private cd: ChangeDetectorRef) {
  }

  ngOnChanges() {
    this.initDataSource();
    if (this.matPaginator) {
      this.matPaginator.length = this.paginator.length;
    }
  }

  ngOnInit(): void {
    this.bulk && this.initBulk();

    this.dataColumns = this.dataColumns.concat(this.displayColumns);
    if (this.expandedRow || this.bulk) {
      this.dataColumns.unshift(ReservedNamesEnum.serviceColumn);
    }
  }

  ngAfterViewInit(): void {
    this.apiable ? this.initApiableTable() : this.initNotApiableTable();
    this.paginator && this.initPaginator();
    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  isFirstColumnPadding(column_name: string): boolean {
    return (this.bulk || this.expandedRow) && this.displayColumns.indexOf(column_name) === 0;
  }

  emitActionRequest(actionType: string, index: number) {
    const action: ActionRequestModel<T> = {
      type: actionType,
      entity: this.data[index]
    };

    this.actionRequest.emit(action);
  }

  massSelectionChange() {
    this.dataSource.data.forEach(element => element.selected = this.massBulkCheckbox.checked);
    this.bulkContainer$.next();
  }

  onRowClicked(element: ParsedObjectModel<T>) {
    this.emitActionRequest('open', element.originalIndex);
  }

  onExpandedClicked(element: ParsedObjectModel<T>) {
    if (this.expandedRow) {
      this.expandedElement = this.expandedElement === element ? null : element;
    }
  }

  private initDataSource() {
    this.dataSource = new MatTableDataSource<ParsedObjectModel<T>>(this.service.getParsedData(this.data, this.columns));
  }

  private initPaginator() {
    this.matPaginator.pageIndex = this.paginator.pageIndex;
    this.matPaginator.pageSize = this.paginator.pageSize;
    this.matPaginator.pageSizeOptions = [5, 10, 25, 50];
    if (this.apiable) {
      this.matPaginator.length = this.paginator.length;
    }
  }

  private disableSort() {
    this.matSort.disabled = true;
  }

  private initBulk() {
    this.bulkContainer$
      .pipe(debounceTime(1000),
        takeUntil(this.onDestroy))
      .subscribe((value?: T[]) => {
          this.bulkContainerUpdate.emit(value ||
            this.dataSource.data
              .filter(element => element.selected)
              .map(element => this.data[element.originalIndex])
          );
        }
      );
  }

  private initApiableTable() {
    const observables: Array<Observable<any>> = [
      fromEvent(this.searchInput.nativeElement, 'keyup')
        .pipe(filter((event: KeyboardEvent) => event.key === 'Enter'),
          map(() => this.searchCtrl.value),
          distinctUntilChanged(),
          takeUntil(this.onDestroy),
          startWith(undefined)),
      this.matPaginator.page
        .pipe(startWith(undefined)),
      this.matSort.sortChange
        .pipe(startWith(undefined))
    ];

    combineLatest(observables)
      .pipe(takeUntil(this.onDestroy),
        filter(([search, paginator, sort]: [string, PageEvent, Sort]) =>
          !(search === undefined && paginator === undefined && sort === undefined)),
        map(([search, paginator, sort]: [string, PageEvent, Sort]): ApiRequestModel => {
          const resPaginator: PaginationRequestModel = paginator || this.paginator;

          const resSort: SortRequestModel = sort ? {
            ...sort,
            direction: sort.direction ? sort.direction.toUpperCase() as UpperCaseSortDirection : null
          } : null;

          return {search, paginator: resPaginator, sort: resSort};
        }))
      .subscribe(apiRequest => {

        if (this.previousSearch && apiRequest.search !== this.previousSearch && this.paginator) {
          apiRequest.paginator.pageIndex = 0;
        }

        if (this.bulk) {
          this.massBulkCheckbox.checked = false;
        }
        this.bulkContainer$.next([]);

        this.previousSearch = apiRequest.search;

        this.apiRequest.emit(apiRequest);
      });
  }

  private initNotApiableTable() {
    this.dataSource.sort = this.matSort;
    // WORKAROUND don't simplify it, sometimes without it paginator doesn't init itself
    setTimeout(() => this.dataSource.paginator = this.matPaginator);

    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(filter((event: KeyboardEvent) => event.key === 'Enter'),
        map(() => this.searchCtrl.value),
        distinctUntilChanged(),
        takeUntil(this.onDestroy))
      .subscribe(value => {
        this.dataSource.filter = value ? value.trim().toLowerCase() : '';

        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      });
  }

}
