import {moduleMetadata, storiesOf} from '@storybook/angular';
import {action} from '@storybook/addon-actions';
import {ColumnsV2Config} from '../lib/mat-table-v2/_shared/configs/columns-v2.config';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BdDatePipeModule} from '@bd-innovations/pipe-collection';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {Component, Inject} from '@angular/core';
import {EXPANDED_ROW_DATA} from '../lib/mat-table-v2/_shared/utilities/table-tokens';
import {PaginationConfig} from '../lib/mat-table-v2/lazy-table/models/paginated-request.model';
import {
  ALL_ACTIONS_MOCK,
  BULK_ACTIONS_MOCK,
  COLUMNS_MOCK,
  COLUMNS_WITH_FILTERS_MOCK,
  DATA_MOCK,
  LOCAL_TRANSLATE_PREFIX_MOCK,
  MODULE_CONFIG_MOCK,
  PAGINATION_MOCK,
  SINGLE_ACTIONS_MOCK,
  TITLE_MOCK
} from './_shared/mocks';
import {fakeTranslateLoaderFactory, FakeTranslateSetupModule} from './_shared/ngx-translate';
import {TableModule} from '../lib/mat-table-v2/table.module';
import {DynamicFormService} from '@bd-innovations/dynamic-form';
import {HttpClientTestingModule} from '@angular/common/http/testing';

const title: string = TITLE_MOCK;
const data: any[] = DATA_MOCK;
const columns: ColumnsV2Config = COLUMNS_MOCK;
const translatePrefix: string = LOCAL_TRANSLATE_PREFIX_MOCK;
const globalSort = false;
const pagination: PaginationConfig = PAGINATION_MOCK;

const handleActionRequest = action('onActionRequestEmmit');
const handleBulkActionRequest = action('onBulkActionRequestEmit');
const handleDisplayColumnsUpdate = action('onDisplayColumnsUpdateEmit');

// @ts-ignore
@Component({
  selector: 'app-expanded',
  template: `
    <ng-container *ngFor="let duty of element?.duties">
      <div>
        <b>Before Sunrise</b>:
        {{duty.beforeSunrise}} -
        <b>After Sunset</b>:
        {{duty.afterSunset}}
      </div>
    </ng-container>`,
  styles: []
})
class ExpandedComponent {

  constructor(@Inject(EXPANDED_ROW_DATA) public element: any) {
  }

}

storiesOf('Regular Table', module)
  .addDecorator(moduleMetadata({
    declarations: [ExpandedComponent],
    entryComponents: [ExpandedComponent],
    imports: [
      BrowserAnimationsModule,
      HttpClientTestingModule,

      BdDatePipeModule.forChild(),

      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: fakeTranslateLoaderFactory,
        },
      }),
      FakeTranslateSetupModule,

      TableModule.forRoot(MODULE_CONFIG_MOCK)
    ],
    providers: [
      DynamicFormService
    ]
  }))
  .add('Basic', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"></bd-table>`,
      props: {
        data,
        columns,
      }
    }),
    {
      notes: 'Basic usage of Material Dynamic Table V2, max default and minimalistic config'
    })
  .add('With Title', () => ({
      template: `<bd-table [title]="title"
                           [data]="data"
                           [columns]="columns"></bd-table>`,
      props: {
        title,
        data,
        columns,
      }
    }),
    {
      notes: ''
    })
  .add('Translated headers', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"
                           [translatePrefix]="translatePrefix"></bd-table>`,
      props: {
        data,
        columns,
        translatePrefix
      }
    }),
    {
      notes: 'Basic usage of Material Dynamic Table V2, but table headers have custom translate paths'
    })
  .add('Disabled sort', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"
                           [globalSort]="globalSort"></bd-table>`,
      props: {
        data,
        columns,
        globalSort
      }
    }),
    {
      notes: 'Basic usage of Material Dynamic Table V2, global sort is disabled'
    })
  .add('With actions', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"
                           [actions]="actions"
                           (actionRequest)="handleActionRequest($event)"></bd-table>`,
      props: {
        data,
        columns,
        actions: {
          ...SINGLE_ACTIONS_MOCK,
          ...ALL_ACTIONS_MOCK
        },
        handleActionRequest
      }
    }),
    {
      notes: ''
    })
  .add('Paginated', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"
                           [pagination]="pagination"
                           (actionRequest)="handleActionRequest($event)"></bd-table>`,
      props: {
        data,
        columns,
        pagination,
        handleActionRequest
      }
    }),
    {
      notes: ''
    })
  .add('Expanded row', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"
                           [expandedComponentRef]="expandedRef"></bd-table>`,
      props: {
        data,
        columns,
        expandedRef: ExpandedComponent
      }
    }),
    {
      notes: 'Example of Material Dynamic Table V2 with Expanded Row'
    })
  .add('Bulk', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"
                           [actions]="actions"
                           (bulkActionRequest)="handleBulkActionRequest($event)"></bd-table>`,
      props: {
        data,
        columns,
        actions: {...BULK_ACTIONS_MOCK},
        handleBulkActionRequest
      }
    }),
    {
      notes: 'Basic usage of Material Dynamic Table V2 with option to work with bulk data'
    })
  .add('Dynamic displayed columns', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"
                           (displayedColumnsUpdate)="handleDisplayColumnsUpdate($event)"></bd-table>`,
      props: {
        data,
        columns,
        handleDisplayColumnsUpdate
      }
    }),
    {
      notes: 'Basic usage of Material Dynamic Table V2 with event for saving configured displayColumns.\n' +
        'Button for configuring columns doesn\'t appear unless there is observer for the event.\n' +
        'Also inner display columns don\'t update themself but wait for columns input to be updated'
    })
  .add('Filters', () => ({
      template: `<bd-table [data]="data"
                           [columns]="columns"></bd-table>`,
      props: {
        data,
        columns: {
          ...columns,
          ...COLUMNS_WITH_FILTERS_MOCK
        },
      }
    }),
    {
      notes: ''
    })
  .add('All in one', () => ({
      template: `<bd-table [title]="title"
                           [data]="data"
                           [columns]="columns"
                           [translatePrefix]="translatePrefix"
                           [actions]="actions"
                           [pagination]="pagination"
                           [expandedComponentRef]="expandedRef"
                           (actionRequest)="handleActionRequest($event)"
                           (bulkActionRequest)="handleBulkActionRequest($event)"
                           (displayedColumnsUpdate)="handleDisplayColumnsUpdate($event)"></bd-table>`,
      props: {
        title,
        data,
        columns: {
          ...columns,
          ...COLUMNS_WITH_FILTERS_MOCK
        },
        translatePrefix,
        actions: {
          ...SINGLE_ACTIONS_MOCK,
          ...ALL_ACTIONS_MOCK,
          ...BULK_ACTIONS_MOCK
        },
        pagination,
        expandedRef: ExpandedComponent,
        handleActionRequest,
        handleBulkActionRequest,
        handleDisplayColumnsUpdate
      }
    }),
    {
      notes: ''
    });
