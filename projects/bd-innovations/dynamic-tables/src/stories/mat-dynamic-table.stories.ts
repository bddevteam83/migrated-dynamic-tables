import {moduleMetadata, storiesOf} from '@storybook/angular';
import {MatDynamicTableModule} from '../lib/mat-dynamic-table/mat-dynamic-table.module';
import * as faker from 'faker';
import {ColumnsConfig} from '../lib/mat-dynamic-table/configs/columns.config';
import {AvatarColumn} from '../lib/mat-dynamic-table/columns/avatar.column';
import {TextColumn} from '../lib/mat-dynamic-table/columns/text.column';
import {DateColumn} from '../lib/mat-dynamic-table/columns/date.column';
import {IconColumn} from '../lib/mat-dynamic-table/columns/icon.column';
import {ActionsConfig} from '../lib/mat-dynamic-table/configs/actions.config';
import {BdDatePipeModule} from '@bd-innovations/pipe-collection';
import {TranslateModule} from '@ngx-translate/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReservedNamesEnum} from '../lib/mat-dynamic-table/models/reserved-names.enum';
import {action} from '@storybook/addon-actions';
import {PaginationRequestModel} from '../lib/mat-dynamic-table/models/api-request.model';
import {DOM_ELEMENT_VALIDATOR_SERVICE_TOKEN} from '@bd-innovations/directive-collection';

const data: any[] = Array.apply(null, {length: 30})
  .map(() => ({
    uuid: faker.random.uuid(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    birthday: faker.date.past(),
    activeUntil: faker.date.future(),
    status: faker.random.boolean(),
    duties: Array.apply(null, {length: 5})
      .map(() => ({
        afterSunset: faker.lorem.sentence(),
        beforeSunrise: faker.lorem.sentence()
      })),
    type: null
  }));

const columns: ColumnsConfig = {
  avatar: new AvatarColumn('firstName'),
  name: new TextColumn(['firstName', 'lastName']),
  birthday: new DateColumn('birthday', {timeAgo: true}),
  activeUntil: new DateColumn('activeUntil'),
  status: new IconColumn('status', {
    icons: [
      {
        case: true,
        icon: '<i class="mdi mdi-checkbox-marked-circle"></i>'
      },
      {
        case: false,
        icon: '<i class="mdi mdi-alert-circle"></i>'
      }
    ]
  }),
  type: new TextColumn('type')
};

const displayColumns: string[] = [
  'avatar',
  'name',
  'birthday',
  'activeUntil',
  'status',
  'type',

  ReservedNamesEnum.actionsColumn
];

const actions: ActionsConfig<any> = [
  'open',
  'edit',
  'delete'
];

const handleActionRequest = action('onActionRequestEmmit');

const paginator: PaginationRequestModel = {
  pageIndex: 0,
  pageSize: 25,
  length: 30
};

const handleApiRequest = action('onApiRequestEmit');

storiesOf('_deprecated/Old Mat Dynamic Table', module)
  .addDecorator(moduleMetadata({
    imports: [
      BrowserAnimationsModule,
      BdDatePipeModule.forChild(),
      TranslateModule.forRoot(),

      MatDynamicTableModule.forRoot({
        searchField: {
          placeholder: 'Search for...',
          icon: '<i class="mdi mdi-magnify"></i>',
          resetIcon: '<i class="mdi mdi-close"></i>'
        },
        actionsColumn: {
          icon: '<i class="mdi mdi-dots-vertical"></i>',
          actions: {
            open: {icon: '<i class="mdi mdi-dots-horizontal"></i>', label: 'Details'},
            edit: {icon: '<i class="mdi mdi-pencil"></i>', label: 'Edit'},
            delete: {icon: '<i class="mdi mdi-delete"></i>', label: 'Delete'}
          }
        },
        expandIcon: '<i class="mdi mdi-chevron-down"></i>'
      }),
    ],
    providers: [
      {provide: DOM_ELEMENT_VALIDATOR_SERVICE_TOKEN, useValue: {}}
    ]
  }))
  .add('Default', () => ({
      template: `<bd-mat-dynamic-table [data]="data"
                                       [columns]="columns"
                                       [displayColumns]="displayColumns"
                                       [actions]="actions"
                                       (actionRequest)="handleActionRequest($event)"></bd-mat-dynamic-table>`,
      props: {
        data,
        columns,
        displayColumns,
        actions,
        handleActionRequest
      }
    }),
    {
      notes: 'Basic usage of Material Dynamic Table, max default and minimalistic config'
    })
  .add('With Expanded Row', () => ({
      template: `<bd-mat-dynamic-table [data]="data"
                                       [columns]="columns"
                                       [displayColumns]="displayColumns"
                                       [actions]="actions"
                                       [expandedRow]="expandedRow"
                                       (actionRequest)="handleActionRequest($event)"></bd-mat-dynamic-table>
                 <ng-template #expandedRow let-element="element">
                    <ng-container *ngFor="let duty of element.duties">
                        <div>
                          <b>Before Sunrise</b>:
                          {{duty.beforeSunrise}} -
                          <b>After Sunset</b>:
                          {{duty.afterSunset}}
                        </div>
                    </ng-container>
                 </ng-template>`,
      props: {
        data,
        columns,
        displayColumns,
        actions,
        handleActionRequest
      }
    }),
    {
      notes: 'Example of Material Dynamic Table with Expanded Row'
    })
  .add('Apiable', () => ({
      template: `<bd-mat-dynamic-table [data]="data"
                                       [columns]="columns"
                                       [displayColumns]="displayColumns"
                                       [actions]="actions"
                                       [apiable]="true"
                                       [paginator]="paginator"
                                       (actionRequest)="handleActionRequest($event)"
                                       (apiRequest)="handleApiRequest($event)"></bd-mat-dynamic-table>`,
      props: {
        data: data.slice(0, 25),
        columns,
        displayColumns,
        actions,
        handleActionRequest,
        paginator,
        handleApiRequest
      }
    }),
    {
      notes: 'Material Dynamic Table which made to work with data throw API requests.' +
        'The main difference between this one and regular table is that it emits api requests' +
        'instead of working with passed \'data\' right away and then suppose the \'data\' input to be changed. ' +
        'Also because of this workflow \'paginator\' should be passed to table to set proper total items length'
    });
