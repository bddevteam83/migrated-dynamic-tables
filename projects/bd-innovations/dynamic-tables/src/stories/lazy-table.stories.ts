import {moduleMetadata, storiesOf} from '@storybook/angular';
import {action} from '@storybook/addon-actions';
import {ColumnsV2Config} from '../lib/mat-table-v2/_shared/configs/columns-v2.config';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BdDatePipeModule} from '@bd-innovations/pipe-collection';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {
  ALL_ACTIONS_MOCK,
  BULK_ACTIONS_MOCK,
  COLUMNS_MOCK,
  COLUMNS_WITH_FILTERS_MOCK,
  DATA_MOCK,
  LOCAL_TRANSLATE_PREFIX_MOCK,
  MODULE_CONFIG_MOCK,
  PAGINATION_MOCK,
  SINGLE_ACTIONS_MOCK,
  TITLE_MOCK
} from './_shared/mocks';
import {fakeTranslateLoaderFactory, FakeTranslateSetupModule} from './_shared/ngx-translate';
import {TableModule} from '../lib/mat-table-v2/table.module';
import {Component, Inject} from '@angular/core';
import {EXPANDED_ROW_DATA} from '../lib/mat-table-v2/_shared/utilities/table-tokens';
import {DynamicFormService} from '@bd-innovations/dynamic-form';
import {HttpClientTestingModule} from '@angular/common/http/testing';

const title: string = TITLE_MOCK;
const paginatedData: any = {list: DATA_MOCK.slice(0, PAGINATION_MOCK.pageSize), totalItems: DATA_MOCK.length};
const columns: ColumnsV2Config = COLUMNS_MOCK;
const translatePrefix: string = LOCAL_TRANSLATE_PREFIX_MOCK;

const handleActionRequest = action('onActionRequestEmmit');
const handleBulkActionRequest = action('onBulkActionRequestEmit');
const handleDisplayColumnsUpdate = action('onDisplayColumnsUpdateEmit');
const handlePaginationRequest = action('onPaginationRequestEmit');

// @ts-ignore
@Component({
  selector: 'app-expanded',
  template: `
    <ng-container *ngFor="let duty of element?.duties">
      <div>
        <b>Before Sunrise</b>:
        {{duty.beforeSunrise}} -
        <b>After Sunset</b>:
        {{duty.afterSunset}}
      </div>
    </ng-container>`,
  styles: []
})
class ExpandedComponent {

  constructor(@Inject(EXPANDED_ROW_DATA) public element: any) {
  }

}

storiesOf('Lazy Table', module)
  .addDecorator(moduleMetadata({
    declarations: [ExpandedComponent],
    entryComponents: [ExpandedComponent],
    imports: [
      BrowserAnimationsModule,
      HttpClientTestingModule,

      BdDatePipeModule.forChild(),

      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: fakeTranslateLoaderFactory,
        }
      }),
      FakeTranslateSetupModule,

      TableModule.forRoot(MODULE_CONFIG_MOCK)
    ],
    providers: [
      DynamicFormService
    ]
  }))
  .add('All in one', () => ({
      template: `<bd-lazy-table [title]="title"
                                [paginatedData]="paginatedData"
                                [columns]="columns"
                                [translatePrefix]="translatePrefix"
                                [actions]="actions"
                                [expandedComponentRef]="expandedRef"
                                (actionRequest)="handleActionRequest($event)"
                                (paginatedBulkActionRequest)="handleBulkActionRequest($event)"
                                (displayedColumnsUpdate)="handleDisplayColumnsUpdate($event)"
                                (paginationRequest)="handlePaginationRequest($event)"></bd-lazy-table>`,
      props: {
        title,
        paginatedData,
        columns: {
          ...columns,
          ...COLUMNS_WITH_FILTERS_MOCK
        },
        translatePrefix,
        actions: {
          ...SINGLE_ACTIONS_MOCK,
          ...ALL_ACTIONS_MOCK,
          ...BULK_ACTIONS_MOCK
        },
        expandedRef: ExpandedComponent,
        handleActionRequest,
        handleBulkActionRequest,
        handleDisplayColumnsUpdate,
        handlePaginationRequest
      }
    }),
    {
      notes: ''
    });
