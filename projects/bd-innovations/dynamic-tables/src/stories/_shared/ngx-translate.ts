import {TranslateFakeLoader, TranslateService} from '@ngx-translate/core';
import {Observable, of} from 'rxjs';
import {NgModule} from '@angular/core';

export class FakeTranslateLoader implements TranslateFakeLoader {
  getTranslation(lang: string): Observable<any> {
    return of({
      title: 'Title',
      'filtered-by': 'Filtered by',
      filter: 'Filter',
      'displayed-columns': 'Displayed columns',
      selected: 'Selected',
      'items-from': 'items from',
      cancel: 'Cancel',
      submit: 'Submit',
      avatar: 'Avatar',
      name: 'Name',
      birthday: 'Birthday',
      activeUntil: 'Active until',
      status: 'Status',
      dataUsage: 'Data usage',
      work: 'Work',
      type: 'Type',
      'global-prefix': {
        'filtered-by': 'Filtered by',
        filter: 'Filter',
        'displayed-columns': 'Displayed columns',
        selected: 'Selected',
        'items-from': 'items from',
        cancel: 'Cancel',
        submit: 'Submit',
      },
      'local-prefix': {
        title: 'Title',
        avatar: 'Avatar',
        name: 'Name',
        birthday: 'Birthday',
        activeUntil: 'Active until',
        status: 'Status',
        dataUsage: 'Data usage',
        work: 'Work',
        type: 'Type'
      }
    });
  }
}

export function fakeTranslateLoaderFactory(): FakeTranslateLoader {
  return new FakeTranslateLoader();
}

@NgModule()
export class FakeTranslateSetupModule {
  constructor(translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('en');
  }
}
