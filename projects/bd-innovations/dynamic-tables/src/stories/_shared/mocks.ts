import * as faker from 'faker';
import {ColumnsV2Config} from '../../lib/mat-table-v2/_shared/configs/columns-v2.config';
import {ActionsV2Config} from '../../lib/mat-table-v2/_shared/configs/actions-v2.config';
import {PaginationConfig} from '../../lib/mat-table-v2/lazy-table/models/paginated-request.model';
import {ModuleV2Config} from '../../lib/mat-table-v2/_shared/configs/module-v2.config';

export const TITLE_MOCK = 'title';

export const MODULE_CONFIG_MOCK: ModuleV2Config = {
  globalIcons: {
    actions: 'mdi mdi-dots-vertical',
    bulkActions: 'mdi mdi-dots-horizontal',
    filter: 'mdi mdi-filter',
    expand: 'mdi mdi-chevron-down',
    displayedColumns: 'mdi mdi-view-parallel',
    displayedColumnsReorder: 'mdi mdi-drag-horizontal-variant',
    removeChip: 'mdi mdi-close',
    // activeChip: 'mdi mdi-check'
  },
  actionIcons: {
    open: {iconClass: 'mdi mdi-dots-horizontal', label: 'Details'},
    edit: {iconClass: 'mdi mdi-pencil', label: 'Edit'},
    delete: {iconClass: 'mdi mdi-delete', label: 'Delete'},
    commit: {iconClass: 'mdi mdi-source-commit', label: 'Commit'}
  },
  paginatorPageSizeOptions: [5, 10, 25, 50],
  globalTranslatePrefix: '',
  nullOrUndefinedPipePlaceholder: '-'
};

export const DATA_MOCK = Array.apply(null, {length: 30})
  .map(() => ({
    uuid: faker.random.uuid(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    birthday: faker.date.past(),
    activeUntil: faker.date.future(),
    status: faker.random.boolean(),
    dataUsage: faker.random.number(100),
    work: {
      company: faker.company.companyName(),
      position: faker.lorem.sentence()
    },
    duties: Array.apply(null, {length: 5})
      .map(() => ({
        afterSunset: faker.lorem.sentence(),
        beforeSunrise: faker.lorem.sentence()
      })),
    type: faker.random.arrayElement(['null', 'not-null'])
  }));

export const COLUMNS_MOCK: ColumnsV2Config = {
  avatar: {
    discriminator: 'AvatarColumn',
    paths: 'firstName'
  },
  name: {
    discriminator: 'TextColumn',
    paths: ['firstName', 'lastName']
  },
  birthday: {
    discriminator: 'DateColumn',
    paths: 'birthday',
    timeAgo: true
  },
  activeUntil: {
    discriminator: 'DateColumn',
    paths: 'activeUntil'
  },
  status: {
    discriminator: 'IconColumn',
    paths: 'status',
    icons: [
      {
        case: true,
        icon: 'mdi mdi-checkbox-marked-circle'
      },
      {
        case: false,
        icon: 'mdi mdi-alert-circle'
      }
    ]
  },
  dataUsage: {
    discriminator: 'ProgressBarColumn',
    paths: 'dataUsage',
    maxValuePaths: '100'
  },
  work: {
    discriminator: 'TextColumn',
    paths: 'work.company',
    subPath: 'work.position'
  }
};

export const LOCAL_TRANSLATE_PREFIX_MOCK = 'local-prefix.';

export const COLUMNS_WITH_FILTERS_MOCK: ColumnsV2Config = {
  type: {
    discriminator: 'TextColumn',
    paths: 'type',
    control: {
      discriminator: 'selection-list',
      options: ['null', 'not-null']
    }
  },
  work: {
    discriminator: 'TextColumn',
    paths: 'work.company',
    subPath: 'work.position',
    control: {
      discriminator: 'selection-list',
      options: Array.apply(null, {length: 5}).map(() => faker.company.companyName())
    }
  }
};

export const SINGLE_ACTIONS_MOCK: ActionsV2Config<any> = {
  open: {type: 'single'},
  edit: {type: 'single'}
};

export const BULK_ACTIONS_MOCK: ActionsV2Config<any> = {
  delete: {type: 'bulk'},
};

export const ALL_ACTIONS_MOCK: ActionsV2Config<any> = {
  commit: {type: 'both'},
};

export const PAGINATION_MOCK: PaginationConfig = {
  pageIndex: 0,
  pageSize: 5
};
