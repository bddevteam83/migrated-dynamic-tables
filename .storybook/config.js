import 'hammerjs';

import "../node_modules/@angular/material/prebuilt-themes/indigo-pink.css";
import "../node_modules/@mdi/font/css/materialdesignicons.css";

import {configure} from '@storybook/angular';

configure(require.context('../projects/bd-innovations/dynamic-tables/src/stories', true, /\.stories\.ts$/), module);
